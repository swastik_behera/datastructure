package LinkedLIst;

public class PallindromeCheck {
    boolean isPalindrome(Node head)
    {
        Node slow=head;
        Node fast=head;

        while(fast!=null && fast.next!=null)
        {
            slow=slow.next;
            fast=fast.next.next;
        }
        Node prev=slow;
        Node currmid=slow.next;
        prev.next=null;
        while(currmid!=null)
        {
            Node nextnode=currmid.next;
            currmid.next=prev;
            prev=currmid;
            currmid=nextnode;
        }
        Node headfromlast=prev;
        while(head!=null && headfromlast!=null)
        {
            if(head.data!=headfromlast.data)
                return false;
            head=head.next;
            headfromlast=headfromlast.next;
        }
        return true;
    }
}
