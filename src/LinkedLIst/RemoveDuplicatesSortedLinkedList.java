package LinkedLIst;

public class RemoveDuplicatesSortedLinkedList {
    Node removeDuplicates(Node head)
    {
        Node pre=head;
        Node curr=head.next;
        while(curr!=null )
        {
            if(pre.data==curr.data)
            {
                pre.next=curr.next;
                Node lastcurr=curr;
                curr=curr.next;
                lastcurr.next=null;
            }
            else {
                pre=curr;
                curr=curr.next;
            }
        }
        return head;
    }
}
