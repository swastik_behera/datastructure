package LinkedLIst;

public class SwapKthNodeFromBeginingfromEnd {
    Node swapkthnode(Node head, int num, int K)
    {
        if(head==null) return head;
        Node fast=head;
        Node slow=head;
        Node nodefromstart=null;
        int count=0;
        while(fast!=null && count<K)
        {
            nodefromstart=fast;
            fast=fast.next;
            count++;
        }
        if(count!=K) return head;
        while(fast!=null)
        {
            slow=slow.next;
            fast=fast.next;
        }
        int temp=nodefromstart.data;
        nodefromstart.data=slow.data;
        slow.data=temp;
        Node curr=head;
        while(curr!=null) System.out.print(curr.data+" ");
        System.out.println();
        return head;
    }
}
