package LinkedLIst;

import java.util.Scanner;

public class MergeSort {
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        int t=sc.nextInt();
        while(t-->0)
        {
            int n=sc.nextInt();
            Node head=new Node(sc.nextInt());
            Node tail=head;
            while(n-- >1)
            {
                tail.next=new Node(sc.nextInt());
                tail=tail.next;
            }
            head=mergeSort(head);
        }
    }
    static Node mergeSort(Node head)
    {
        if(head==null || head.next==null) return head;
        Node slow=head;
        Node fast=head;
        Node beforemid=null;
        while(fast!=null && fast.next!=null)
        {
            beforemid=slow;
            slow=slow.next;
            fast=fast.next.next;
        }
        beforemid.next=null;
        Node firsthalf=mergeSort(head);
        Node secondhalf=mergeSort(slow);
        return merge(firsthalf,secondhalf);

    }

    private static Node merge(Node firsthalf, Node secondhalf) {
        if(firsthalf==null) return secondhalf;
        if(secondhalf==null) return firsthalf;
        Node tailnode=null;
        Node head=null;
        if(firsthalf.data<=secondhalf.data)
        {
            head=firsthalf;
            tailnode=firsthalf;
            firsthalf=firsthalf.next;
        }
        else{
            head=secondhalf;
            tailnode=secondhalf;
            secondhalf=secondhalf.next;
        }
        while(firsthalf!=null && secondhalf!=null)
        {
            if(firsthalf.data<=secondhalf.data)
            {
               tailnode.next=firsthalf;
               tailnode=firsthalf;
               firsthalf=firsthalf.next;
            }
            else{
                tailnode.next=secondhalf;
                tailnode=secondhalf;
                secondhalf=secondhalf.next;
            }
        }
        if(firsthalf==null) tailnode.next=secondhalf;
        else tailnode.next=firsthalf;
        return head;
    }
}

