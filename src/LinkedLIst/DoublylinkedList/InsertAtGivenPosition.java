package LinkedLIst.DoublylinkedList;

public class InsertAtGivenPosition {
    void addNode(Node head_ref, int pos, int data)
    {
        Node curr=head_ref;
        Node newnode=new Node(data);
        int k=0;
        while(k++<pos)
            curr=curr.next;
        if(curr.next==null)
        {
            curr.next=newnode;
            newnode.prev=curr;
            return;
        }
        newnode.next=curr.next;
        newnode.prev= curr;
        curr.next=newnode;
        newnode.next.prev=newnode;
    }
}
