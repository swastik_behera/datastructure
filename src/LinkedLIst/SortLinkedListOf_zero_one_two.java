package LinkedLIst;

public class SortLinkedListOf_zero_one_two {

    static Node segregate(Node head)
    {
        Node zerostart=null;
        Node zeroend=null;
        Node onestart=null;
        Node oneend=null;
        Node twostart=null;
        Node twoend=null;
        Node curr=head;
        while(curr!=null)
        {
            Node prev=curr;
            curr=curr.next;
            if(prev.data==0)
            {
                if(zerostart==null){
                    zerostart=prev;
                    zeroend=prev;
                }
                else {
                    zeroend.next=prev;
                    zeroend=prev;
                }
            }
            else if(prev.data==1)
            {
                if(onestart==null){
                    onestart=prev;
                    oneend=prev;
                }
                else{
                    oneend.next=prev;
                    oneend=prev;
                }
            }
            else{
                if(twostart==null)
                {
                    twostart=prev;
                    twoend=prev;
                }
                else{
                    twoend.next=prev;
                    twoend=prev;
                }
            }
        }
        if(zerostart!=null)
        {
            head=zerostart;
            if(onestart!=null)
            {
                zeroend.next=onestart;
                if(twostart!=null) {
                    oneend.next=twostart;
                    twoend.next=null;
                }
                else oneend.next=null;
            }
            else{
                zeroend.next=twostart;
                if(twostart!=null) twoend.next=null;
            }
        }
        else{
            if(onestart!=null)
            {
                head=onestart;
                if(twostart!=null){
                    oneend.next=twostart;
                    twoend.next=null;
                }
                else oneend.next=null;
            }
            else{
                head=twostart;
                twoend=null;
            }
        }
        return head;
    }
}
