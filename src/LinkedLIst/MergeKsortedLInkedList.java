package LinkedLIst;

public class MergeKsortedLInkedList {
    Node mergeKList(Node[]head,int N)
    {
        Node finalhead=head[0];
        MergeKsortedLInkedList obj=new MergeKsortedLInkedList();
        for(int i=1;i<N;i++)
        {
            finalhead=obj.merge(finalhead,head[i]);
        }
        return finalhead;
    }

    private Node merge(Node first, Node second) {
        if(first == null) return second;
        if(second == null) return second;
        Node head=null;
        Node tail=null;
        if(first.data<=second.data)
        {
            head=first;
            tail=first;
            first=first.next;
        }
        else{
            head=second;
            tail=second;
            second=second.next;
        }
        while(first!=null && second!=null)
        {
            if(first.data<=second.data)
            {
                tail.next=first;
                tail=first;
                first=first.next;
            }
            else{
                tail.next=second;
                tail=second;
                second=second.next;
            }
        }
        if(first==null) tail.next=second;
        else tail.next=first;
        return head;
    }
}
