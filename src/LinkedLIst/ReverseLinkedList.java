package LinkedLIst;

public class ReverseLinkedList {
    Node reverseList(Node head)
    {
        if(head==null) return head;
        Node curr=head.next;
        Node prev=head;
        prev.next=null;
        while(curr!=null)
        {
            Node currentcurr= curr;
            curr=curr.next;
            currentcurr.next=prev;
            prev=currentcurr;
        }
        head=prev;
        return head;
    }
}
