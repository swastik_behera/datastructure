package LinkedLIst;

public class CloneAlinkedLIst {

    Node copyList(Node head) {

        if (head == null) return head;
        Node curr = head;
        while (curr != null) {
            Node copy = new Node(curr.data);
            Node nextnode = curr.next;
            curr.next = copy;
            copy.next = nextnode;
            curr = nextnode;
        }
        curr = head;
        while (curr != null) {
            if (curr.arb != null)
                curr.next.arb = curr.arb.next;
            curr = curr.next.next;
        }
        curr = head.next;
        head.next = null;
        head = curr;
        while (curr != null && curr.next != null) {
            Node nextnode = curr.next.next;
            curr.next.next = null;
            curr.next = nextnode;
            curr = nextnode;
        }
        return head;
    }
}
