package LinkedLIst;

public class NthNodeFromLinkedList {

    int getNthFromLast(Node head, int n)
    {
        if(head==null) return -1;
        Node slow=head;
        Node fast=head;
        int count=0;
        while(fast!=null && count<n)
        {
            fast=fast.next;
            count++;
        }
        if(count!=n) return -1;
        while(fast!=null)
        {
            slow=slow.next;
            fast=fast.next;
        }
        if(slow!=null)
         return slow.data;
        return -1;
    }
}
