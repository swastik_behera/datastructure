package LinkedLIst;

public class IntersectionPointOfYshapedLInkedList {

    int intersectPoint(Node head1, Node head2)
    {
        if(head1==null || head2==null) return -1;
        if(head1==head2) return head1.data;
        Node curr1=head1;
        Node curr2=head2;
        int c1=0;
        int c2=0;
        while(curr1!=null)
        {
            c1++;
            curr1=curr1.next;
        }
        while (curr2!=null)
        {
            c2++;
            curr2=curr2.next;
        }
        int diff=Math.abs(c1-c2);
        if(c1>=c2)
        {
            curr1=head1;
            curr2=head2;
        }
        else{
            curr1=head2;
            curr2=head1;
        }
        int count=0;
        while(count<diff)
        {
            curr1=curr1.next;
            count++;
        }
        while(curr1!=null && curr2!=null)
        {
            if(curr1==curr2) return curr1.data;
            curr1=curr1.next;
            curr2=curr2.next;
        }
        return -1;
    }

}
