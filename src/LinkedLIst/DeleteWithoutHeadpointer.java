package LinkedLIst;

public class DeleteWithoutHeadpointer {
    void deleteNode(Node node)
    {
        node.data=node.next.data;
        Node next=node.next;
        node.next=node.next.next;
        next.next=null;
    }
}
