package LinkedLIst;

public class PairwiseSwap {

    public static Node pairwise_swap(Node head) {
        if (head == null || head.next == null) return head;
        Node curr = head.next.next;
        Node prev = head;
        head = head.next;
        head.next = prev;
        while (curr != null && curr.next != null) {
            prev.next = curr.next;
            prev = curr;
            Node nextnode = curr.next.next;
            curr.next.next = curr;
            curr = nextnode;
        }
        prev.next = curr;
        return head;
    }

}
