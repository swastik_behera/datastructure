package LinkedLIst;

public class CountNoOfNodes {

    public static int getCount(Node head)
    {
        if(head==null) return 0;
        int count=0;
        Node curr=head;
        while (curr!=null) {
            count++;
            curr=curr.next;
        }
        return count;
    }
}
