package LinkedLIst;

public class RotateLinkedList {

    public Node rotate(Node head, int k) {
         if(head==null || k==0) return head;
         Node kth=head;
         Node curr=head;
         Node endNode=null;
         int count=0;
         while(curr!=null && count<k)
         {
             kth=curr;
             curr=curr.next;
             count++;
         }
         if(curr==null && count==k) return head;
         while(curr!=null)
         {
             endNode=curr;
             curr=curr.next;
         }
         endNode.next=head;
         head=kth.next;
         kth.next=null;
         return head;
    }
}
