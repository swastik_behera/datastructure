package LinkedLIst;

import java.util.HashSet;

public class RemoveDuplicatesUnsortedLinkedList {

    public Node removeDuplicates(Node head)
    {
        HashSet<Integer> set=new HashSet<>();
        Node prev=head;
        set.add(prev.data);
        Node curr=head.next;
        while(curr!=null)
        {
            if(set.contains(curr.data))
            {
                Node lastcurr=curr;
                prev.next= curr.next;
                curr=curr.next;
                lastcurr.next=null;
            }
            else{
                set.add(curr.data);
                prev=curr;
                curr=curr.next;
            }
        }
        return head;
    }
}
