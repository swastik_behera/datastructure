package LinkedLIst;

public class LoopLenthCount {
    int countNodesinLoop(Node head)
    {
        if(head==null) return 0;
        Node slow=head;
        Node fast=head;
        while(fast!=null && fast.next!=null)
        {
            slow=slow.next;
            fast=fast.next.next;
            if(slow==fast)
                break;
        }
        if(slow!=fast) return 0;
        fast=fast.next;
        int count=1;
        while(fast!=slow) {
            fast = fast.next;
            count++;
        }
        return count;
    }
}
