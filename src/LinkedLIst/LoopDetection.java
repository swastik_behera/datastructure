package LinkedLIst;

public class LoopDetection {
    public int detectLoop(Node head) {
        if(head==null) return 0;
        Node slow=head;
        Node fast=head;
        while(fast!=null && fast.next!=null)
        {
            slow=slow.next;
            fast=fast.next.next;
            //above conditon will come before thatn checking
            if(slow==fast)
                return 1;

        }
        return 0;
    }
}
