package LinkedLIst;

public class InsertIntoLinkedList {

    public static void main(String[] args) {
        InsertIntoLinkedList obj=new InsertIntoLinkedList();
        obj.insertAtBeginning(9);
        obj.printList();
        obj.insertAtEnd(5);
        obj.printList();
        obj.insertAtEnd(6);
        obj.printList();
        obj.insertAtBeginning(2);
        obj.printList();
        obj.insertAtBeginning(5);
        obj.printList();

    }

    Node head; // Please do not remove this
    Node tail;
    // Should insert a node at the beginning
    void insertAtBeginning(int x)
    {
        Node curr=new Node(x);
        if(head==null)
            tail=curr;
        curr.next=head;
        head=curr;
    }

    // Should insert a node at the end
    void insertAtEnd(int x)
    {
        Node curr=new Node(x);
        if(head==null) {
            tail = curr;
            curr.next=head;
            head=curr;
        }
        else {
            tail.next=curr;
            tail=curr;
        }
    }
    // Please do not delete this
    void printList()
    {
        Node temp = head;
        while (temp != null)
        {
            System.out.print(temp.data+" ");
            temp = temp.next;
        }
        System.out.println();
    }
}
