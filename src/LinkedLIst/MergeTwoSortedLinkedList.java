package LinkedLIst;

public class MergeTwoSortedLinkedList {

    Node sortedMerge(Node headA, Node headB) {
        if(headA==null) return headB;
        if(headB==null) return headA;
        Node curr1=headA;
        Node curr2=headB;
        Node tail=null;
        Node head=null;
        if(curr1.data<=curr2.data)
        {
            head=curr1;
            tail=curr1;
            curr1=curr1.next;
        }
        else {
            head=curr2;
            tail=curr2;
            curr2=curr2.next;
        }
        tail.next=null;
        while(curr1!=null && curr2!=null)
        {
            if(curr1.data<=curr2.data)
            {
                tail.next=curr1;
                tail=tail.next;
                curr1=curr1.next;
            }
            else{
                tail.next=curr2;
                tail=tail.next;
                curr2=curr2.next;
            }
            tail.next=null;
        }
        if(curr1==null) tail.next=curr2;
        else tail.next=curr1;
        return head;
    }
}
