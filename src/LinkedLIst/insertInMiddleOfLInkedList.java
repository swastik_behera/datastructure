package LinkedLIst;

public class insertInMiddleOfLInkedList {
    public Node insertInMid(Node head, int data){
        Node slow=head;
        Node fast=head.next;
        Node node1=new Node(data);
        while(fast!=null && fast.next!=null)
        {
            slow=slow.next;
            fast=fast.next.next;
        }
        node1.next=slow.next;
        slow.next=node1;
        return head;
    }
}
