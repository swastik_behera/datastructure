package BitWise;

public class FirstSetBit {
    public static void main(String[] args) {
        System.out.println(getFirstSetBitPos(64));
    }
    public static int getFirstSetBitPos(int n){
        int start=1;
        int k=0;
        while((n & (1<<k))==0)
            k++;
        return k+1;
    }
}
