package BitWise;

public class countSetBits {
    public static void main(String[] args) {
        System.out.println(countSetBits(12));
    }
    public static int countSetBits(int n){
        n+=1;
        int count=0;

        for( int x=2 ; x/2<n ; x=x*2 )
        {
            int quotient = n/x;
            count += quotient * x / 2;

            int remainder = n%x;
            if(remainder > x/2)
                count += remainder - x/2;
        }

        return count;
    }
}
