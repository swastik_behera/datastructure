package BitWise;

public class BitDifference {
    public static void main(String[] args) {
        System.out.println(countBitsFlip(20,25));
    }

    public static int countBitsFlip(int a, int b){
        int xorEle=a ^ b;
        int count=0;
        while(xorEle >0)
        {
            xorEle=xorEle & (xorEle-1);
            count++;
        }

        return count;
    }

}
