package BitWise;

public class CheckKthBitSet {
    static boolean checkKthBit(int n, int k){
        int kthsetNum= 1<<k;
        if((n&kthsetNum)!=0)
            return true;
        return false;
    }
}
