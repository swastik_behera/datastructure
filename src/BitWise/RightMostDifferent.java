package BitWise;

public class RightMostDifferent {
    public static void main(String[] args) {
        System.out.println(posOfRightMostDiffBit(52,4));
    }
    public static int posOfRightMostDiffBit(int m, int n) {
        int k = 0;
        int num=m ^ n;
        while ((num & (1 << k))==0)
            k++;
        return k+1;
    }
}
