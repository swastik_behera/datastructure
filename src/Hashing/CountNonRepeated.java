package Hashing;

import java.util.HashMap;
import java.util.Map;

public class CountNonRepeated {


    public int countNonRepeated(int arr[], int n) {
        int result = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for (int x : arr) {
            if (map.containsKey(x))
                map.put(x, map.get(x) + 1);
            else
                map.put(x, 1);
        }
        for (Map.Entry entry : map.entrySet()) {
            int x = (Integer) entry.getValue();
            if (x < 2)
                result++;
        }
        return result;
    }

}
