package Hashing;

import java.util.TreeSet;

public class LongestConsecutiveSubSequence {
    public static void main(String[] args) {
        System.out.println(findLongestConseqSubseq(new int[]{1 ,9 ,3 ,10 ,4 ,20 ,2},7));
    }
    static int findLongestConseqSubseq(int arr[], int n)
    {
        TreeSet<Integer> set=new TreeSet<>();
        for(int x:arr)
            set.add(x);
        int longest=0;
        int count=1;
        int next=-1;
        for(int x:set)
        {
            if(x==next)
                count++;
            else
                count=1;
            next = x + 1;
            if(count>longest)
                longest=count;

        }
        return longest;
    }
}
