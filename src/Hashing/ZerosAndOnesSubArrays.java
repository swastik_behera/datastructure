package Hashing;

import java.util.HashMap;

public class ZerosAndOnesSubArrays {
    public static void main(String[] args) {

    }
    static int countSubarrWithEqualZeroAndOne(int arr[], int n)
    {
        int count=0;
        HashMap<Integer,Integer> map=new HashMap<>();
        int presum=0;
        map.put(0,1);
        for(int x:arr)
        {
            if(x==0)
                presum+=-1;
            else presum+=1;
            if(map.containsKey(presum))
            {
                int v=map.get(presum);
                count+=v;
                map.put(presum,v+1);
            }
            else map.put(presum,1);
        }
        return count;
    }
}
