package Hashing;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class RelativeSorting {
    public static void main(String[] args) throws Exception{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        int t=Integer.valueOf(br.readLine().trim());
        while(t-->0)
        {
            String[] sizes=br.readLine().split(" ");
            int n=Integer.valueOf(sizes[0]);
            int m=Integer.valueOf(sizes[1]);
            String[] input1=br.readLine().split(" ");
            String[] input2=br.readLine().split(" ");
            TreeMap<Integer,Integer> map=new TreeMap<>();
            StringBuffer result=new StringBuffer();
            for(String str:input1)
            {
                int num=Integer.valueOf(str);
                if(map.containsKey(num))
                    map.put(num,map.get(num)+1);
                else map.put(num,1);
            }
            for(String str:input2)
            {
                int num=Integer.valueOf(str);
                if(map.containsKey(num)) {
                    int v = map.get(num);
                    while (v-- > 0)
                        result.append(num+" ");
                    map.remove(num);
                }
            }
            for(Map.Entry<Integer,Integer> entry:map.entrySet())
            {
                int v=entry.getValue();
                while(v-->0)
                    result.append(entry.getKey()+" ");
            }
            System.out.print(result);
            System.out.println();
        }
    }
}
