package Hashing;

import java.util.HashSet;
import java.util.Set;

public class PairSerch1 {
    public static void main(String[] args) {

    }
    public static int sumExists(int arr[], int sizeOfArray, int sum)
    {
        Set<Integer> set=new HashSet<>();
        boolean isPresent=false;
        for(int x:arr)
        {
            if(set.contains(sum-x))
                isPresent=true;
            else set.add(x);

        }
        if(isPresent)
            return 1;
        return 0;
    }
}
