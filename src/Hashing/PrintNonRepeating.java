package Hashing;

import java.util.LinkedHashMap;
import java.util.Map;

public class PrintNonRepeating {

    public static void main(String[] args) {

    }

    static void printNonRepeated(int arr[], int n)
    {
        Map<Integer,Integer> map=new LinkedHashMap<>();
        for(int x:arr)
        {
            if(map.containsKey(x))
                map.put(x,map.get(x)+1);
            else
                map.put(x,1);
        }
        for(Map.Entry<Integer,Integer> entry:map.entrySet())
        {
            int x=(Integer)entry.getValue();
            if(x<2) System.out.print(entry.getKey()+" ");
        }
    }
}
