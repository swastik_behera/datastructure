package Hashing;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TwoArrayEquality {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.valueOf(br.readLine());
        while (t-- > 0) {
            int n = Integer.valueOf(br.readLine());
            String[] inp1 = br.readLine().split(" ");
            String[] inp2 = br.readLine().split(" ");
            List<Long> set1 = new ArrayList<>();
            for (int i = 0; i < n; i++)
                set1.add(Long.valueOf(inp1[i]));
            for (int i = 0; i < n; i++)
                set1.remove(Long.valueOf(inp2[i]));
            if (set1.size() > 0) System.out.print(0);
            else System.out.print(1);
            System.out.println();
        }
    }

}
