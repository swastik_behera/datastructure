package Hashing;

import java.util.Map;
import java.util.TreeMap;

public class WinnerOfElection {
    public static void main(String[] args) {

    }
    static void winner(String arr[], int n)
    {
        String winner=null;
        int maxVote=0;
        Map<String,Integer> map=new TreeMap();
        for(String name:arr)
        {
            if(map.containsKey(name))
                map.put(name,map.get(name)+1);
            else map.put(name,1);
        }
        for(Map.Entry<String,Integer> entry:map.entrySet())
        {
            if(entry.getValue()>maxVote)
            {
                maxVote=entry.getValue();
                winner=entry.getKey();
            }
        }
        System.out.println(winner+" "+maxVote);
    }
}
