package Hashing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class FirstRepeatingElement {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.valueOf(br.readLine());
        while (t-- > 0) {
            int size = Integer.valueOf(br.readLine());
            int[] arr = new int[size];
            Set<Integer> set = new HashSet<>();
            String[] str = br.readLine().split(" ");
            int index = Integer.MAX_VALUE;
            for (int i = size - 1; i >= 0; i--) {
                if (set.contains(Integer.valueOf(str[i])) && i < index)
                    index = i;
                else
                    set.add(Integer.valueOf(str[i]));
            }
            if(index==Integer.MAX_VALUE)
                index=-2;
            System.out.print(index+1);
            System.out.println();
        }
    }

}
