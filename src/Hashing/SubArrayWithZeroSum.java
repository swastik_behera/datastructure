package Hashing;

import java.util.HashSet;
import java.util.Set;

public class SubArrayWithZeroSum {

    public static void main(String[] args) {

    }

    static boolean findsum(int arr[],int n)
    {
       Set<Integer> set=new HashSet<>();
       set.add(0);
       int presum=0;
       for(int x:arr)
       {
           presum+=x;
           if(set.contains(presum))
               return true;
           else set.add(presum);
       }
       return false;
    }
}
