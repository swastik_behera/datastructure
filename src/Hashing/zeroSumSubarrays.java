package Hashing;

import jdk.internal.util.xml.impl.Input;

import javax.jnlp.IntegrationService;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;

public class zeroSumSubarrays {
    public static void main(String[] args) throws Exception{

        BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
        int t=Integer.valueOf(br.readLine());
        while(t-->0)
        {
            int n= Integer.valueOf(br.readLine().trim());
            String[] input=br.readLine().split(" ");
            HashMap<Integer,Integer> map=new HashMap<>();
            map.put(0,1);
            int presum=0;
            int count=0;
            for(String str:input)
            {
                int num=Integer.valueOf(str);
                presum+=num;
                if(map.containsKey(presum))
                {
                    int v=map.get(presum);
                    count+=v;
                    map.put(presum,v+1);
                }
                else map.put(presum,1);
            }
            System.out.print(count);
            System.out.println();
        }

    }

}
