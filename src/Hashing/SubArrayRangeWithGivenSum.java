package Hashing;

import java.util.HashMap;
import java.util.HashSet;

public class SubArrayRangeWithGivenSum {
    public static void main(String[] args) {

//        System.out.println(subArraySum(new int[]{10 ,2 ,-2 ,-20 ,10},5,-10));
        System.out.println(Math.abs(3-3));
    }
    static int subArraySum(int arr[], int n, int sum)
    {
        HashMap<Integer,Integer> hm = new HashMap<>();
        int count = 0;
        hm.put(0,1);
        int presum=0;
        for(int x:arr)
        {
            presum+=x;
            if(hm.containsKey(presum-sum))
                count+=hm.get(presum-sum);
            if(hm.containsKey(presum))
                hm.put(presum,hm.get(presum)+1);
            else hm.put(presum,1);
        }
        return count;
    }
}
