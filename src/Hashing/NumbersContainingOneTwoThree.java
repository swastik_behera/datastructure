package Hashing;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NumbersContainingOneTwoThree {
    public static void main(String[] args)throws Exception {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        int t=Integer.valueOf(br.readLine());
        while(t-->0)
        {
            int n=Integer.valueOf(br.readLine().trim());
            int arr[]={1,2,3};
            HashSet<Integer> hs = new HashSet<>();
            createHashSet(hs);
            List<Integer> list=new ArrayList<>();
            String[] input=br.readLine().split(" ");
            for(int i=0;i<n;i++)
            {
                if(hs.contains(Integer.valueOf(input[i])))
                    list.add(Integer.valueOf(input[i]));
            }
            if(list.size()>0) {
                for (int x : list) {
                    System.out.print(x+" ");
                    System.out.println();
                }
            }
            else System.out.println(-1);


        }
    }

    public static void createHashSet(HashSet<Integer> hs){
        ArrayList<Integer> l = new ArrayList<>();
        l.add(1);l.add(2);l.add(3);
        int i = 0;
        int j = 3;int count = 1;
        // push the elements which have digits as 1, 2, and 3 only
        while(hs.size()<4200){
            for(int k= i; k<j; k++){
                int num = l.get(k);
                hs.add(num);
                l.add(num*10+1);l.add(num*10+2);l.add(num*10+3);
            }
            i = j;
            j = i + (int)Math.pow(3,count);
            count++;
        }
    }
    /*static Set<Integer> createHashSet(int[] arr,int start)
    {
        Set<Integer> set=new HashSet<>();
        if(start==2) {
            set.add(arr[start]);
            return set;
        }
        for(int i=start;i<3;i++)
        {
            int temp=arr[start];
            arr[start]=arr[i];
            arr[i]=temp;
            int s1=start;
            int num=0;
            while(s1<3)
            {
                num+=arr[s1]*Math.pow(10,2-s1);
                s1++;
            }
            set.add(num);
            set.addAll(createHashSet(arr,start+1));
            temp=arr[start];
            arr[start]=arr[i];
            arr[i]=temp;
        }
        return set;
    }*/

}
