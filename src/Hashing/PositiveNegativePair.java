package Hashing;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.TreeSet;

public class PositiveNegativePair {
    public static void main(String[] args) throws Exception{
        BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
        int t=Integer.valueOf(br.readLine());
        while(t-->0)
        {
            int n=Integer.valueOf(br.readLine());
            String[] input=br.readLine().split(" ");
            HashSet<Integer> set1=new HashSet<>();
            TreeSet<Integer> set2=new TreeSet<>();
            for(String i:input)
            {
                int num=Integer.valueOf(i);
                if(num<0 && set1.contains(Math.abs(num)))
                {
                    int absnum=Math.abs(num);
                    set2.add(Math.abs(absnum));
                    set1.remove(Math.abs(absnum));
                }
                else if(num>0 && set1.contains(num*-1))
                {
                    set2.add(Math.abs(num));
                    set1.remove(num*-1);
                }
                else if(!set1.contains(num))
                    set1.add(num);
            }
            if(set2.size()>0) {
                for (int x : set2)
                    System.out.print(x + " " + (x * -1) + " ");
            }
            else System.out.println(0);
            System.out.println();
        }
    }

}
