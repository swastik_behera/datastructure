package Hashing;

import java.util.*;

public class SortArrayByFrequency {
    public static void main(String[] args) {
        sortByFreq(new int[]{5 ,5 ,4 ,6 ,4},5);
     /*   TreeSet<Hashing.Pair> set=new TreeSet<>();
        Hashing.Pair p1=new Hashing.Pair(6,1);
        Hashing.Pair p2=new Hashing.Pair(5,2);
        Hashing.Pair p3=new Hashing.Pair(4,2);
        set.add(p1);
        set.add(p2);
        set.add(p3);
        for(Hashing.Pair p:set)
        System.out.println(p);*/

    }
    static void sortByFreq(int arr[], int n)
    {
        HashMap<Integer,Pair> map=new HashMap<>();
        for(int x:arr)
        {
            if(map.containsKey(x))
            {
                Pair p=map.get(x);
                p.count++;
            }
            else{
                map.put(x,new Pair(x,1));
            }
        }
        Collection<Pair> values=  map.values();
        TreeSet<Pair> set=new TreeSet<>(values);
        StringBuffer result=new StringBuffer();
        for(Pair p:set)
        {
            int v=p.count;
            while(v-->0)
                result.append(p.value+" ");
        }
        System.out.print(result);
        System.out.println();

    }
}
class Pair implements Comparable<Pair>{
    int value;
    int count;

    public Pair(int value, int count) {
        this.value = value;
        this.count = count;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    @Override
    public int compareTo(Pair o) {
        if(this.count>o.count)
            return -1;
        else if(this.count==o.count)
            return this.value-o.value;
        else return 1;
    }

    @Override
    public String toString() {
        return "Hashing.Pair{" +
                "value=" + value +
                ", count=" + count +
                '}';
    }
}
