package Hashing;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.SQLOutput;
import java.util.HashSet;
import java.util.Set;

public class IntersectionOfTwoArrays {
    public static void main(String[] args) throws Exception{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        int t=Integer.valueOf(br.readLine());
        while(t-->0)
        {
            String[] sizes=br.readLine().split(" ");
            int n=Integer.valueOf(sizes[0]);
            int m=Integer.valueOf(sizes[1]);
            Set<Integer> set1=new HashSet<>();
            Set<Integer> set2=new HashSet<>();
            String[] input1=br.readLine().split(" ");
            String[] input2=br.readLine().split(" ");
            for(int i=0;i<n;i++) set1.add(Integer.valueOf(input1[i]));
            for(int i=0;i<m;i++) set2.add(Integer.valueOf(input2[i]));
            set1.addAll(set2);
            System.out.print(set1.size());
            System.out.println();
        }
    }
}
