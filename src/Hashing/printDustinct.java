package Hashing;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;

public class printDustinct {
    public static void main(String[] args) throws Exception{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        int t=Integer.valueOf(br.readLine());
        while(t-->0)
        {
            int n=Integer.valueOf(br.readLine().trim());
            String[] input=br.readLine().split(" ");
            LinkedHashMap<Integer,Integer> map=new LinkedHashMap<>();
            for(String str:input)
            {
                int num=Integer.valueOf(str);
                if(map.containsKey(num))
                    map.put(num,map.get(num)+1);
                else map.put(num,1);
            }
            for(Map.Entry<Integer,Integer> entry:map.entrySet())
            {
                if(entry.getValue()==1)
                System.out.print(entry.getKey()+" ");
            }
            System.out.println();
        }
    }
}
