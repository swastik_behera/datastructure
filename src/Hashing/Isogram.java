package Hashing;/*Given a string S of lowercase aplhabets,
        check if it is isogram or not. An Hashing.Isogram is a string in which no letter occurs more than once*/

import java.util.HashSet;

public class Isogram {
    static boolean isIsogram(String data){
        HashSet<Character> set=new HashSet<>();
        for(char c:data.toCharArray())
        {
            if(set.contains(c))
                return false;
            else set.add(c);
        }
        return true;
    }
}
