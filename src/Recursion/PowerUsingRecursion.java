package Recursion;

public class PowerUsingRecursion {

    static int RecursivePower(int n,int p)
    {
        if(p==1) return n;
        return n* RecursivePower(n,p-1);
    }
}
