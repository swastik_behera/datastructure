package Recursion;

import java.util.ArrayList;

public class PossibleWordFromPhoneNumber {
    public static void main(String[] args) {
        int[] arr= new int[]{2,3,3};
        ArrayList<String> list=possibleWords(arr,arr.length);
        System.out.println(list);
    }
    static ArrayList<String> possibleWords(int a[], int N)
    {
       ArrayList<String> list=new ArrayList<>();
       extractwords(a,0,list,"");
       return list;
    }

    private static void extractwords(int[] a, int startIndex, ArrayList<String> list,String str) {
        if(startIndex==a.length)
        {
            list.add(str);
            return;
        }
        String target="";
        int num=a[startIndex];
        switch (num){
            case 2:
                target="abc";
                break;
            case 3:
                target="def";
                break;
            case 4:
                target="ghi";
                break;
            case 5:
                target="jkl";
                break;
            case 6:
                target="mno";
                break;
            case 7:
                target="pqrs";
                break;
            case 8:
                target="tuv";
                break;
            case 9:
                target="wxyz";
                break;
            default:
                target="";
        }
        if(target.length()==0)
            extractwords(a,startIndex+1,list,str);
        else {
            for(int i=0;i<target.length();i++)
            {
                str=str+target.charAt(i);
                extractwords(a,startIndex+1,list,str);
                str=str.substring(0,str.length()-1);
            }
        }

    }

}
