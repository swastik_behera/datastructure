package Recursion;

import java.util.Arrays;
import java.util.TreeSet;

public class StringPermutation {
    public static void main(String[] args) {
        new StringPermutation().permutation("ABSG");
    }
    public void permutation(String S)
    {
        char[] str= S.toCharArray();
        TreeSet<String> set=new TreeSet<>();
        permute(str,0,"",set);
        for(String s:set)
            System.out.print(s+" ");
    }

    private void permute(char[] str, int start, String s,TreeSet<String> set) {
        if(start==str.length)
        {
            set.add(s);
            return;
        }

        for(int i=start;i<str.length;i++)
        {
            char temp=str[start];
            str[start]=str[i];
            str[i]=temp;
            s=s+str[start];
            permute(str,start+1,s,set);
            s=s.substring(0,s.length()-1);
            temp=str[start];
            str[start]=str[i];
            str[i]=temp;
        }
    }
}
