package Recursion;

public class DigitalRoot {
    public static void main(String[] args) {
        System.out.print(digitalRoot(99999));
    }
    public static int digitalRoot(int n)
    {
        if(n<10) return n;
        int sum=n%10+digitalRoot(n/10);
        if(sum>10)
            return sum%10+digitalRoot(sum/10);
        return sum;
    }
}
