package Matrix;

public class Multiplication {

    static void multiplyMatrix(int n1, int m1, int n2, int m2, int arr1[][], int arr2[][])
    {
        if(m1!=n2)
        {
            System.out.print(-1);
            return;
        }

        for(int i=0;i<n1;i++)
        {
            for(int j=0;j<m2;j++)
            {
                int mul=0;
                for(int k=0;k<m1;k++)
                    mul+=arr1[i][k]*arr2[k][j];
                System.out.print(mul+" ");
            }
        }
    }
}
