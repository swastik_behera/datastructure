package Matrix;

public class RotateBy90 {
    static void rotateby90(int a[][], int n)
    {
        transpose(a,n);
        for(int j=0;j<n;j++)
        {
            int low=0;
            int high=n-1;
            while(low<high)
            {
                int temp=a[low][j];
                a[low][j]=a[high][j];
                a[high][j]=temp;
            }

        }
    }

    private static void transpose(int[][] a, int n) {

        for(int i=0;i<n;i++)
        {
            for(int j=i+1;j<n;j++)
            {
                int temp=a[i][j];
                a[i][j]=a[j][i];
                a[j][i]=temp;
            }
        }
    }
}
