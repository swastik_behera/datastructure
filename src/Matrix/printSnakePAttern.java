package Matrix;

public class printSnakePAttern {

    static void print(int a[][], int n)
    {
        for(int i=0;i<a.length;i++)
        {
            if(i%2==0)
            {
                for(int j=0;j<a[i].length;j++)
                    System.out.print(a[i][j]+" ");
            }
            else{
                for(int j=a[i].length-1;j>=0;j--)
                    System.out.print(a[i][j]+" ");
            }
        }
    }
}
