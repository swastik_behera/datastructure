package Matrix;

public class SumOfUpperAndLowerTriangle {
    static void Summatrix(int mat[][], int n)
    {
        int uppersum=0;
        int lowersum=0;
        for(int i=0;i<mat.length;i++)
        {
            for(int j=0;j<=i;i++)
                lowersum+=mat[i][j];
            for(int j=i;j<mat[i].length;j++)
                uppersum+=mat[i][j];
        }
        System.out.print(uppersum+" "+lowersum);
    }
}
