package Matrix;

public class SpiralTraversal {
    public static void main(String[] args) {
        int[][] arr=new int [][]{
                {1,2,3,4},{5,6,7,8},{9,10,11,12}
        };
        spirallyTraverse(3,4,arr);
    }
    static void spirallyTraverse(int m, int n, int arr1[][])
    {
        boolean fromleft=true;
        int start=0;
        while(m>0 || n>0)
        {
            for(int i=start;i<n;i++)
                System.out.print(arr1[start][i]+" ");
            for(int i=start+1;i<m;i++)
                System.out.print(arr1[i][n-1]+" ");
            n--;
            if(m>1)
            {
                for(int i=n-2;i>=start;i--)
                    System.out.print(arr1[m-1][i]+" ");
                m--;
            }
            if(n>1)
            {
                for(int i=m-2;i>start;i--)
                    System.out.print(arr1[i][start]+" ");

            }
            start+=1;
        }
    }
}
