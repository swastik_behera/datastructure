package Matrix;

public class BoundaryTraversal {

    static void boundaryTraversal(int n1, int m1, int a[][])
    {
        for(int i=0;i<m1;i++)
            System.out.print(a[0][i]+" ");
        for(int i=1;i<n1;i++)
            System.out.print(a[i][m1-1]+" ");
        if(n1>1) {
            for (int i = m1 - 2; i >= 0; i--)
                System.out.print(a[n1 - 1][i] + " ");
        }
        if(m1>1) {
            for (int i = n1 - 2; i >= 1; i--)
                System.out.print(a[i][0] + " ");
        }

    }
}
