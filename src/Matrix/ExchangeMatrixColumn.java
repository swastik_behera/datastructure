package Matrix;

public class ExchangeMatrixColumn {

    static void exchangeColumns(int n1, int m1, int arr1[][])
    {
        for(int i=0;i<n1;i++)
        {
            int temp=arr1[i][0];
            arr1[i][0]=arr1[i][m1-1];
            arr1[i][m1-1]=temp;
        }
        for(int i=0;i<n1;i++)
        {
            for (int j=0;j<m1;j++)
                System.out.print(arr1[i][j]+" ");
        }
    }
}
