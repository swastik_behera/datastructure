package Matrix;

public class ReverseColumnsOFMatrix {

    static void reverseCol(int n1, int m1, int arr1[][])
    {
        int start=0;
        int end=m1-1;
        while(start<end)
        {
            for(int i=0;i<n1;i++)
            {
                int temp=arr1[i][start];
                arr1[i][start]=arr1[i][end];
                arr1[i][end]=temp;
            }
            start++;
            end--;
        }
    }
}
