package Matrix;

public class SerchInRowColumnSorted {
    static int search(int n1, int m1, int arr1[][], int x)
    {
        int row=0;
        int col=m1-1;
        while(row<n1 && col>=0)
        {
            if(arr1[row][col]==x)
                return 1;
            else if(x>arr1[row][col])
                row++;
            else col--;
        }
        return 0;
    }
}
