package Queue;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class ReverseFirstKElements {

    public Queue<Integer> modifyQueue(Queue<Integer> q, int k)
    {
        Stack<Integer> s=new Stack<>();
        int p=0;
        while(q.isEmpty()==false && p<k)
        {
            s.push(q.poll());
            p++;
        }
        Queue<Integer> res=new LinkedList<>();
        while(s.isEmpty()==false)
            res.add(s.pop());
        while(q.isEmpty()==false)
            res.add(q.poll());
        return  res;
    }
}
