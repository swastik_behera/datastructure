package Queue;

public class ImplementQueueUsingArray {
    int front, rear;
    int arr[] = new int[100005];
    int size;
    ImplementQueueUsingArray()
    {
        front=0;
        rear=0;
        size=0;
    }

    void push(int x)
    {
        arr[rear]=x;
        size++;
        rear=rear+1;
    }

    int pop()
    {
        int x=-1;
        if(size>0) {
            x = arr[front];
            front = front + 1;
        }
        return x;
    }
}
