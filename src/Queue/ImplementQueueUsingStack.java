package Queue;

import java.util.Stack;

public class ImplementQueueUsingStack {
    Stack<Integer> s1 = new Stack<Integer>();
    Stack<Integer> s2 = new Stack<Integer>();

    void Push(int x)
    {
        s1.push(x);
    }

    int Pop()
    {
        int x=-1;
        while(s1.isEmpty()==false)
            s2.push(s1.pop());
        if(s2.isEmpty()==false)
            x=s2.pop();
        while(s2.isEmpty()==false)
            s1.push(s2.pop());
        return x;
    }
}
