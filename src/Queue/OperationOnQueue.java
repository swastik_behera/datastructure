package Queue;

import java.util.LinkedList;
import java.util.Queue;

public class OperationOnQueue {
    Queue<Integer> q=new LinkedList<>();
    void enqueue(int x){
        q.add(x);
    }
    void dequeue(){
        if(q.isEmpty()==false)
            q.poll();
    }
    int front(){
        return q.peek();
        // Code here
    }
    String find(int x){
        Queue<Integer> q1=new LinkedList<>();
        boolean found=false;
        while(q.isEmpty()==false)
        {
            if(q.peek()==x)
                found=true;
            q1.add(q.poll());
        }
        q=q1;
        return (found)?"Yes":"No";
    }
}
