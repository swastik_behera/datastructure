package Queue;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class GenerateBinaryNUmbers {
    static ArrayList<String> generate(long n)
    {
        Queue<String> q=new LinkedList<>();
        ArrayList<String> res=new ArrayList<>();
        q.add("1");
        for(int i=0;i<n;i++)
        {
            String x=q.poll();
            res.add(x);
            q.add(x+"0");
            q.add(x+"1");
        }
        return res;
    }
}
