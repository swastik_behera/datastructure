package Queue;

public class ImplementQueueUsingLInkedlist {
    QueueNode front, rear;

    void push(int a)
    {
        QueueNode node=new QueueNode(a);
        if(front==null)
        {
            front=node;
            rear=node;
        }
        else {
            rear.next=node;
            rear=node;
        }
    }

    int pop()
    {
        int x=-1;
        if(front!=null)
        {
            x=front.data;
            front=front.next;
        }
        return x;
    }
}
