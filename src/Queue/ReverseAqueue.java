package Queue;

import java.util.Queue;

public class ReverseAqueue {
    public Queue<Integer> rev(Queue<Integer> q){

        if(q.isEmpty()) return q;
        int x=q.poll();
        Queue<Integer> q1=rev(q);
        q.add(x);
        return q1;
    }
}
