package Queue;

public class CircularTour {

    int tour(int petrol[], int distance[])
    {
        int n=petrol.length;
        int curr_petrol=0;
        int pre_petrol=0;
        int start=0;
        for(int i=0;i<n;i++)
        {
            curr_petrol+=petrol[i]-distance[i];
            if(curr_petrol<0)
            {
                start=i+1;
                pre_petrol+=curr_petrol;
                curr_petrol=0;
            }
        }
        return (curr_petrol+pre_petrol>0)?(start+1):-1;
    }
}
