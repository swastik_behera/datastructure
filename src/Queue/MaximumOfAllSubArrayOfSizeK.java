package Queue;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;

public class MaximumOfAllSubArrayOfSizeK {
    static ArrayList<Integer> max_of_subarrays(int arr[], int n, int k) {
        Deque<Integer> dq = new LinkedList<>();
        ArrayList<Integer> res = new ArrayList<>();
        for (int i = 0; i < k; i++) {
            if (dq.isEmpty()) dq.add(i);
            else {
                while (dq.isEmpty() == false && arr[dq.peekLast()] <= arr[i])
                    dq.pollLast();
                dq.addLast(i);
            }
        }
        for (int i = k; i < n; i++) {
            res.add(arr[dq.peekFirst()]);
            while (dq.isEmpty() == false && dq.peekFirst() <= i - k)
                dq.pollFirst();
            while (dq.isEmpty() == false && arr[dq.peekLast()] <= arr[i])
                dq.pollLast();
            dq.addLast(i);
        }
        res.add(arr[dq.peekFirst()]);
        return res;
    }
}

