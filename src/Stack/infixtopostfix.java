package Stack;



import org.jetbrains.annotations.NotNull;

import java.util.Stack;

public class infixtopostfix {
    public static void main(String[] args) {
        System.out.println(infixToPostfix("a+b*(c^d-e)^(f+g*h)-i"));
    }

    public static String infixToPostfix(@NotNull String exp) {
        Stack<Character> s=new Stack<>();
        int start='A'+0;
        int end='z'+0;
        String res="";
        for(int i=0;i<exp.length();i++) {
            char c=exp.charAt(i);
            if(c!='^' && c+0>=start && c+0<=end)
                res+=c;
            else if(c=='(')
                s.push(c);
            else if(c==')')
            {
                while(s.peek()!='(')
                    res+=s.pop();
                s.pop();
            }
            else{
                while(s.isEmpty()==false && s.peek()!='('&& isOfLowerPrecedence(s.peek(),c))
                    res+=s.pop();
                s.push(c);
            }
        }
        while(s.isEmpty()==false)
            res+=s.pop();
        return res;
    }

    private static boolean isOfLowerPrecedence(Character peek, char c) {
        if(peek=='^') return true;
        else if(peek=='*' && c!='^') return true;
        else if(peek=='/' &&  c!='^') return true;
        else if(peek=='+' && (c!='*'&& c!='/' && c!='^')) return true;
        else if(peek=='-' && (c!='*'&& c!='/' && c!='^')) return true;
        else return false;
    }
}
