package Stack;

import java.util.Stack;

public class nextLargerElement {
    public static void main(String[] args) {
        long[] res=nextLargerElement(new long[]{6 ,8 ,0 ,1 ,3},5);
        for(long x:res) System.out.print(x+" ");
    }
    public static long[] nextLargerElement(long[] arr, int n) {
        Stack<Long> s=new Stack<>();
        long[] res=new long[n];
        res[n-1]=-1;
        s.push(arr[n-1]);
        for(int i=n-2;i>=0;i--)
        {
            while(s.isEmpty()==false && s.peek()<=arr[i])
                s.pop();
            res[i]=(s.isEmpty())?-1:s.peek();
            s.push(arr[i]);
        }
        return res;
    }
}
