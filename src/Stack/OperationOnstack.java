package Stack;

import java.util.Stack;

public class OperationOnstack {
    public static void insert(Stack<Integer> st, int x)
    {
        st.push(x);
    }

    // Function to pop element from stack
    public static void remove(Stack<Integer> st)
    {
        int x=-1;
        if(st.isEmpty()==false)
                x = st.pop();

    }

    // Function to return top of stack
    public static void headOf_Stack(Stack<Integer> st)
    {
        int x=st.peek();
        System.out.println(x + " ");
    }

    // Function to find the element in stack
    public static boolean find(Stack<Integer> st, int val)
    {
        boolean exists = false;
        Stack<Integer> tempstack=new Stack<>();
        while(st.isEmpty()==false)
        {
            if(st.peek()==val) {
                exists = true;
                break;
            }
            tempstack.push(st.pop());
        }
        while(tempstack.isEmpty()==false)
        {
            st.push(tempstack.pop());
        }

        if(exists == true){
            return true;
        }
        else{
            return false;
        }

    }
}
