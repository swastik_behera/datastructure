package Stack;

import java.util.Stack;

public class CelebrityProblem {
    // The task is to complete this function
    int getId(int M[][], int n)
    {
        Stack<Integer> s=new Stack<>();
        for(int i=0;i<n;i++)
            s.push(i);
        while(s.size()>1)
        {
            int p1=s.pop();
            int p2=s.pop();
            if(M[p1][p2]==1) s.push(p2);
            else s.push(p1);
        }
        if(s.isEmpty()) return -1;
        int p=s.pop();
        for(int i=0;i<n;i++)
        {
            if(i!=p &&(M[p][i]==1 || M[i][p]==0)) return -1;
        }
        return p;
    }
}
