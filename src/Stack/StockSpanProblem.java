package Stack;

import java.util.Stack;

public class StockSpanProblem {
    public static void main(String[] args) {
     int [] res=calculateSpan(new int[]{10 ,4 ,5 ,90 ,120 ,80},6);
     for(int x:res)
         System.out.print(x+" ");

    }
    public static int[] calculateSpan(int price[], int n)
    {
        int[] res=new int[n];
        Stack<Integer> s=new Stack<>();
        res[0]=1;
        s.push(0);
        for(int i=1;i<n;i++)
        {
            while(s.isEmpty()==false && price[s.peek()]<=price[i])
                s.pop();
            if(s.isEmpty())
                res[i]=i+1;
            else res[i]=i-s.peek();
            s.push(i);
        }
        return res;
    }
}
