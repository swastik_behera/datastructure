package Stack;

import java.util.Stack;

public class MaximumOFminimumOfEVeryWindow {
    public static void main(String[] args) {
        int[] res=printMaxOfMin(new int[]{10 ,20 ,30},3);
        for(int x:res)
            System.out.print(x+" ");
    }
    static int[] printMaxOfMin(int[] arr, int N) {
        Stack<Integer> ls=new Stack<>();
        Stack<Integer> rs=new Stack<>();
        int[] ans=new int[N];
        int[] left=new int[N];
        int right[]=new int[N];
        left[0]=-1;
        ls.push(0);
        for(int i=1;i<N;i++)
        {
            while(ls.isEmpty()==false && arr[ls.peek()]>=arr[i])
                ls.pop();
            left[i]=(ls.isEmpty())?-1:ls.peek();
            ls.push(i);
        }
        right[N-1]=N;
        rs.push(N-1);
        for(int i=N-2;i>=0;i--)
        {
            while(rs.isEmpty()==false && arr[rs.peek()]>=arr[i])
                rs.pop();
            right[i]=(rs.isEmpty())?N:rs.peek();
            rs.push(i);
        }
        for(int i=0;i<N;i++)
        {
            int len=right[i]-left[i]-1;
            ans[len-1]=Math.max(ans[len-1],arr[i]);
        }
        for(int i=N-2;i>=0;i--)
        {
            ans[i]=Math.max(ans[i],ans[i+1]);
        }
        return ans;
    }
}
