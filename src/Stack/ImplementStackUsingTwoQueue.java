package Stack;

import java.util.LinkedList;
import java.util.Queue;

public class ImplementStackUsingTwoQueue {

    Queue<Integer> q1 = new LinkedList<>();
    Queue<Integer> q2 = new LinkedList<>();

    /*The method pop which return the element poped out of the stack*/
    int pop()
    {
        int x=-1;
        if(q1.size()>0)
        {
            while(q1.size()>1)
            {
                q2.add(q1.poll());
            }
            x=q1.poll();
            Queue<Integer> temp=q1;
            q1=q2;
            q2=temp;
        }
        return x;
    }

    /* The method push to push element into the stack */
    void push(int a)
    {
        q1.add(a);
    }
}
