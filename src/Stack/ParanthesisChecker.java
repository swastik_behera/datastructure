package Stack;

import java.util.Stack;

public class ParanthesisChecker {
    public static void main(String[] args) {
        System.out.println(ispar("{([])}"));
    }
    static boolean ispar(String x)
    {
        Stack<Character> s=new Stack<>();
        for(int i=0;i<x.length();i++)
        {
            char c=x.charAt(i);
            if(c=='{' || c=='[' || c=='(')
                s.push(c);
            else {
                if (i == 0 && s.isEmpty()) return false;
                if (isClosing(c, s.peek()))
                    s.pop();
                else return false;
            }
        }
        if(s.isEmpty()) return true;
        return false;
    }

    private static boolean isClosing(char c, Character peek) {
        if(peek=='{' && c=='}') return true;
        else if(peek=='[' && c==']') return true;
        else if(peek=='(' && c==')') return true;
        else return false;
    }
}

