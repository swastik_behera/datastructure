package Stack;

import java.util.Stack;

public class EvaluatePostFix {

    public static int evaluatePostFix(String exp){
        Stack<Integer> s=new Stack<>();
        int start='0'+0;
        int end='9'+0;
        for(char c:exp.toCharArray())
        {
            if(c+0>=start && c+0<=end)
            {
                int x=Integer.parseInt(c+"");
                s.push(x);
            }
            else{
                int x=s.pop();
                int y=s.pop();
                s.push(calculate(c,x,y));
            }
        }
        return s.pop();
    }

    private static int calculate(char c, int x, int y) {
        if(c=='+') return x+y;
        else if(c=='*') return x*y;
        else if(c=='/') return x/y;
        else return x-y;
    }

}
