package Stack;

import java.util.Map;
import java.util.Stack;

public class MaximumHistogramArea {
    public static void main(String[] args) {
        System.out.println(getMaxArea(new long[]{7 ,2 ,8 ,9 ,1 ,3 ,6 ,5},8));
    }
    public static long getMaxArea(long hist[], long n)  {
        long res=0;
        Stack<Integer> s=new Stack<>();
        for(int i=0;i<n;i++)
        {
            while(s.isEmpty()==false && hist[s.peek()]>hist[i])
            {
                int index=s.pop();
                long area=hist[index]*((s.isEmpty())?i:i-s.peek()-1);
                res=Math.max(res,area);
            }
            s.push(i);


        }
        while(s.isEmpty()==false)
        {
            int index=s.pop();
            long area=hist[index]*((s.isEmpty())?n:n-s.peek()-1);
            res= Math.max(res,area);
        }
        return res;
    }
}
