package Stack;

import java.util.Stack;

public class DeleteMiddleElementOfStack {
    public static void main(String[] args) {
        Stack<Integer> s=new Stack<>();
        s.push(1);
        s.push(2);
        s.push(3);
        s.push(4);
        s.push(5);
        deleteMid(s,5,0);
    }
    public static Stack<Integer> deleteMid(Stack<Integer>s, int sizeOfStack, int current)
    {
        if(current==sizeOfStack/2) {
            s.pop();
            return s;
        }
        int poped=s.pop();
        s=deleteMid(s,sizeOfStack,current+1);
        s.push(poped);
        return s;
    }
}
