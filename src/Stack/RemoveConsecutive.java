package Stack;

import java.util.Stack;

public class RemoveConsecutive {
    public static void main(String[] args) {
        System.out.println(removeConsecutiveDuplicates("aaaab"));
    }
    public static String removeConsecutiveDuplicates(String str){
        String res="";
        for(int i=0;i<str.length();)
        {
            char c=str.charAt(i);
            int j=i+1;
            for(j=i+1;j<str.length();j++)
            {
                if(str.charAt(j)!=c)
                {
                    res=res+c;
                    break;
                }
            }
            if(j==str.length())
                res=res+c;
            i=j;
        }
        return res;
    }
}
