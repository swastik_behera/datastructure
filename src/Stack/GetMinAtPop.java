package Stack;

import java.util.Stack;

public class GetMinAtPop {

    static Stack<Integer> minstack= new Stack<>();

    public static Stack<Integer> _push(int arr[], int n)
    {
       Stack<Integer> s=new Stack<>();
       for(int x:arr)
       {
           s.push(x);
           if(minstack.isEmpty()) minstack.push(x);
           else if(x<=minstack.peek()) minstack.push(x);
       }
       return s;
    }

    /* print minimum element of the stack each time
       after popping*/
    static void _getMinAtPop(Stack<Integer>s)
    {
        while(s.isEmpty()==false) {
            int min = minstack.peek();
            if (min == s.peek())
                minstack.pop();
            s.pop();
            System.out.print(min+" ");
        }
    }
}
