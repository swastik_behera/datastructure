package Stack;

public class ImplementUsingLinkedList {
    StackNode top;

    void push(int a) {
        StackNode node=new StackNode(a);
        node.next=top;
        top=node;
    }
    int pop() {
        int res=-1;
        if(top!=null)
        {
            StackNode node=top;
            top=top.next;
            res=node.data;
            node.next=null;
        }
        return res;
    }
}

class StackNode
{
    int data;
    StackNode next;
    StackNode(int a)
    {
        data = a;
        next = null;
    }
}
