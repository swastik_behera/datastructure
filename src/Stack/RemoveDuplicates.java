package Stack;

import java.util.Stack;

public class RemoveDuplicates {
    public static void main(String[] args) {
        System.out.println(removePair("aaabbaaccd"));
    }
    public static String removePair(String str){
        Stack<Character> s=new Stack<>();
        for(int i=0;i<str.length();i++)
        {
            if(s.isEmpty()==false && s.peek()==str.charAt(i))
                s.pop();
            else s.push(str.charAt(i));
        }
        String res="";
        while(s.isEmpty()==false)
            res=s.pop()+res;
        return (res.length()>0)?res:"Empty String";
    }
}
