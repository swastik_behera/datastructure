package Array;

public class StockBuyAndSell {
    public static void main(String[] args) {
        stockBuySell(new int[]{57 ,69 ,12 ,59 ,5 ,9 ,29 ,29, 99},9);
    }
    static void stockBuySell(int price[], int n) {
        if(n==1) System.out.print("No Profit");
        Pair[] pair=new Pair[n/2+1];
        int count=0;
        int i=0;
        while(i<n)
        {
            while(i<n-1 && price[i]>=price[i+1])
                i++;
            if(i==n-1)
                break;
            pair[count]=new Pair();
            pair[count].buy=i++;
            while(i<n && price[i]>=price[i-1])
                i++;
            pair[count].sell=i-1;
            count++;
        }
        if(count==0)
            System.out.print("No Profit");
        else{
            int k=0;
            while(k<count){
                System.out.print("("+pair[k].buy+" "+pair[k].sell+") ");
                k++;
            }

        }


    }
}
class Pair{
    int buy;
    int sell;
}
