package Array;

public class EquilibriumPoint {
    public static int equilibriumPoint(long arr[], int n) {
        long sum=0;
        for (long x:arr)
            sum+=x;
        long leftsum=0;
        for(int i=0;i<n;i++)
        {
            if(leftsum==sum-arr[i])
                return i+1;
            leftsum+=arr[i];
            sum-=arr[i];
        }
        return -1;
    }
}
