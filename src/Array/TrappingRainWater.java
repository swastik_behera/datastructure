package Array;

public class TrappingRainWater {
    public static void main(String[] args) {
        System.out.println(trappingWater(new int[]{3,0,5,2,0,4},6));
    }
    static int trappingWater(int arr[], int n) {
        int leftMax[] =new int[n];
        int rightMax[] =new int[n];
        leftMax[0]=arr[0];
        rightMax[n-1]=arr[n-1];
        for(int i=1;i<n;i++)
            leftMax[i]=Math.max(leftMax[i-1],arr[i]);
        for(int i=n-2;i>=0;i--)
            rightMax[i]=Math.max(rightMax[i+1],arr[i]);
        int total=0;
        for(int i=0;i<n-1;i++)
        {
            int minval=Math.min(leftMax[i],rightMax[i]);
            if(arr[i]<minval) {
                total += minval - arr[i];
            }
        }
        return total;
    }
}
