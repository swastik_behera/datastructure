package Array;

public class SmallestPositiveMissing {
    public static void main(String[] args) {
        System.out.println(missingNumber(new int[]{27 ,-13 ,46 ,-32 ,7 ,23 ,12 ,-5 ,-46 ,43 ,-25 ,-43 ,-15 ,-4 ,-45 ,-1 ,-45 ,-2 ,3 ,23 ,31 ,-11 ,-11 ,26 ,41 ,27 ,41 ,14 ,-20 ,-28, 5 ,-42 ,10 ,1 ,-24 ,19 ,27 ,-10 ,-35},39));
    }
    static int missingNumber(int arr[], int size)
    {
        int counter=1;
        int j=0;
        int i=0;
        while(i<size)
        {
            if(arr[i]<=0) {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                j++;
            }
            i++;
        }
        int[] positives=new int[size-j];
        for(i=j;i<size;i++)
        {
           if(j+arr[i]-1<size)
               positives[arr[i]-1]=-1;
        }
        for(i=0;i<positives.length;i++)
        {
            if(positives[i]>=0)
                return i+1;
            counter++;
        }
        return counter;

    }
}
