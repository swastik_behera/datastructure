package Array;

public class KadaneAlgorithm {
    public static void main(String[] args) {
        System.out.println(new KadaneAlgorithm().maxSubarraySum(new int[]{-3,-1,9,1},4));
    }
    int maxSubarraySum(int arr[], int n){

        int maxh = 0, maxf = Integer.MIN_VALUE;
        for(int i=0; i<n; i++){
            maxh+=arr[i];
            maxf=Integer.max(maxh,maxf);
            if(maxh<0)
                maxh=0;

        }

        return maxf;

    }
}
