package Array;

public class RearrangeArray_1 {
    public static void main(String[] args) {
        long[] arr=new long[]{4 ,0 ,2 ,1 ,3};
        arrange(arr,5);
        for(long x:arr)
            System.out.print(x+" ");
    }
    static void arrange(long arr[], int n)
    {
        int max_ele=n;
        for(int i=0;i<n;i++)
        {
            arr[i]+=(arr[(int)arr[i]]%max_ele)*max_ele;
        }
        for(int i=0;i<n;i++)
            arr[i]=arr[i]/max_ele;
    }
}
