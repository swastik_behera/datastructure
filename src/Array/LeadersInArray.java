package Array;

import java.util.ArrayList;
import java.util.Collections;

public class LeadersInArray {

    static ArrayList<Integer> leaders(int arr[], int n){

        ArrayList<Integer> res=new ArrayList<>();
        res.add(arr[n-1]);
        int max=arr[n-1];
        for(int i=n-2;i>=0;i--)
        {
            if(arr[i]>=max)
            {
                res.add(arr[i]);
                max=arr[i];
            }
        }
        Collections.reverse(res);
        return res;
    }
}
