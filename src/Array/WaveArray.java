package Array;

public class WaveArray {

    public static void convertToWave(int arr[], int n){
       int i=0;
       while(i<n)
       {
           if(i+1<n)
           {
                int temp=arr[i];
                arr[i]=arr[i+1];
               arr[i+1]=temp;
           }
           i=i+2;
       }
    }
}
