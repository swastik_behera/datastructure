package Array;

public class MInimumAbsoluteAdjacent {
    public static void main(String[] args) {
        System.out.println(minAdjDiff(new int[]{8 ,-8 ,9 ,-9 ,10 ,-11 ,12},7));
    }
    public static int minAdjDiff(int arr[], int n) {
        int minimum=Integer.MAX_VALUE;
        for(int i=0;i<n;i++)
            minimum=Math.min(minimum,Math.abs(arr[i]-arr[(i+1)%n]));
        return minimum;
    }
}
