package Array;

import javax.jnlp.ClipboardService;
import java.util.ArrayList;

public class SecondMAx {
    public static ArrayList<Integer> largestAndSecondLargest(int sizeOfArray, int arr[])
    {
            int secondmax=-1;
            int max=arr[0];
            for(int i=1;i<sizeOfArray;i++)
            {
                if(arr[i]>max)
                {
                    secondmax=max;
                    max=arr[i];
                }
               else if(arr[i]>secondmax && arr[i]<max)
                    secondmax=arr[i];
            }
            ArrayList<Integer> list=new ArrayList<>();
            list.add(max);
            list.add(secondmax);
            return list;
    }
}
