package Array;

public class MaximumOccuredInRange {
    public static int maxOccured(int L[], int R[], int n, int maxx){

        int[] arr= new int[1000000];
        for(int i=0;i<n;i++)
        {
            arr[L[i]]++;
            arr[R[i]+1]--;
        }
        int count=arr[0];
        int element=0;
        for(int i=1;i<1000000;i++)
        {
            arr[i]+=arr[i-1];
            if(arr[i]>count)
            {
                count=arr[i];
                element=i;
            }
        }
        return element;
    }
}
