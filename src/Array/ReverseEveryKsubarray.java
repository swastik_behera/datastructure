package Array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReverseEveryKsubarray {
    public static void main(String[] args) {
        List<Integer> input=Arrays.asList(1 ,2 ,3 ,4 ,5 ,6 ,7 ,8);
        List<Integer> res=reverseInGroups(input,8,3);
        for(int x:res)
            System.out.print(x+" ");
    }
    public static List<Integer> reverseInGroups(List<Integer> mv, int n, int k) {
        int i=0;
        while(i<n)
        {
            int low=i;
            int high=0;
            if(i+k-1<n)
                high=i+k-1;
            else high=n-1;
            while(low<high)
            {
                int temp=mv.get(low);
                mv.set(low,mv.get(high));
                mv.set(high,temp);
                low++;
                high--;
            }
            i=i+k;
        }
        return mv;
    }
}
