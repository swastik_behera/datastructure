package Array;

public class RotateCounterClockwise {

    static void rotateArr(int arr[], int d, int n)
    {
        rotate(arr,0,d-1);
        rotate(arr,d,n-1);
        rotate(arr,0,n-1);
    }

    private static void rotate(int[] arr, int low, int high) {
        while(low<high)
        {
            int temp=arr[low];
            arr[low]=arr[high];
            arr[high]=temp;
            low++;
            high--;
        }

    }

}
