package Array;

public class Reverse {
    public static void arrayReversal(int arr[],int sizeOfArray)
    {
        int low=0;
        int high=sizeOfArray-1;
        while(low<high)
        {
            int temp=arr[low];
            arr[low]=arr[high];
            arr[high]=temp;
            low++;
            high--;
        }
    }
}
