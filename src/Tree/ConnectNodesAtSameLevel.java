package Tree;

import java.util.LinkedList;
import java.util.Queue;

public class ConnectNodesAtSameLevel {
    public static void connect(Node root)
    {
        if(root==null) return;
        Queue<Node> q=new LinkedList<>();
        q.add(root);
        while(q.isEmpty()==false)
        {
            int count=q.size();
            Node previous=null;
            for(int i=0;i<count;i++)
            {
                Node curr=q.poll();
                if(i==0)
                    previous=curr;
                else
                    previous.nextRight=curr;
                if(curr.left!=null) q.add(curr.left);
                if(curr.right!=null) q.add(curr.right);
            }
        }

    }
}
