package Tree;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

public class CountNUmberOFSubTreeHavingGivenSum {
    int count=0;
    int countSubtreesWithSumX(Node root, int X)
    {
        getcount(root,X);;
        return count;
    }

    private int getcount(Node root, int sum) {
        if(root==null) return Integer.MIN_VALUE;
        int left=getcount(root.left,sum);
        int right=getcount(root.right,sum);
        int localsum=0;
        if(left!=Integer.MIN_VALUE)
            localsum+=left;
        if(right!=Integer.MIN_VALUE)
            localsum+=right;
        if((root.data+localsum)==sum) count++;
        return root.data+localsum;
    }
    static Node buildTree(String str){

        if(str.length()==0 || str.charAt(0)=='N'){
            return null;
        }

        String ip[] = str.split(" ");
        // Create the root of the tree
        Node root = new Node(Integer.parseInt(ip[0]));
        // Push the root to the queue

        Queue<Node> queue = new LinkedList<>();

        queue.add(root);
        // Starting from the second element

        int i = 1;
        while(queue.size()>0 && i < ip.length) {

            // Get and remove the front of the queue
            Node currNode = queue.peek();
            queue.remove();

            // Get the current node's value from the string
            String currVal = ip[i];

            // If the left child is not null
            if(!currVal.equals("N")) {

                // Create the left child for the current node
                currNode.left = new Node(Integer.parseInt(currVal));
                // Push it to the queue
                queue.add(currNode.left);
            }

            // For the right child
            i++;
            if(i >= ip.length)
                break;

            currVal = ip[i];

            // If the right child is not null
            if(!currVal.equals("N")) {

                // Create the right child for the current node
                currNode.right = new Node(Integer.parseInt(currVal));

                // Push it to the queue
                queue.add(currNode.right);
            }
            i++;
        }

        return root;
    }
    static void printInorder(Node root)
    {
        if(root == null)
            return;

        printInorder(root.left);
        System.out.print(root.data+" ");

        printInorder(root.right);
    }

    public static void main (String[] args) throws IOException {
        BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

        int t=Integer.parseInt(br.readLine());
        while(t-- > 0){
            String s= br.readLine();
            Node root = buildTree(s);


            int x=Integer.parseInt(br.readLine());
            CountNUmberOFSubTreeHavingGivenSum tr = new CountNUmberOFSubTreeHavingGivenSum();
            System.out.println(tr.countSubtreesWithSumX(root,x));

        }

    }
}
