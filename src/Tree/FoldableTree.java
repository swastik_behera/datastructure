package Tree;

public class FoldableTree {
    boolean IsFoldable(Node root)
    {
        if(root==null) return true;
        Node left=null;
        Node right=null;
        if(root.left!=null) left=root.left;
        if(root.right!=null) right=root.right;
        if(left==null && right ==null ) return true;
        if(left==null || right==null) return false;
        return foldable(left,right);

    }

    private boolean foldable(Node leftNode, Node rightNode) {
        if(leftNode==null && rightNode ==null ) return true;
        if(leftNode==null || rightNode==null) return false;
        return foldable(leftNode.left,rightNode.right) && foldable(leftNode.right,rightNode.left);
    }
}
