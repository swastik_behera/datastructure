package Tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class LevelOrder {
    static ArrayList<Integer> levelOrder(Node node)
    {
        ArrayList<Integer> list=new ArrayList<>();
        if(node==null) return list;
        Queue<Node> q=new LinkedList<>();
        q.add(node);
        while(q.isEmpty()==false)
        {
            Node curr=q.poll();
            list.add(curr.data);
            if(curr.left!=null) q.add(curr.left);
            if(curr.right!=null) q.add(curr.right);
        }
        return list;
    }
}
