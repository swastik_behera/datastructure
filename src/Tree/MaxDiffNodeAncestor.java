package Tree;

public class MaxDiffNodeAncestor {
    int maxdiff=Integer.MIN_VALUE;
    int maxDiff(Node root)
    {
        getMaxDIff(root);
        return maxdiff;
    }

    private int getMaxDIff(Node root) {
        if(root==null) return Integer.MAX_VALUE;
        int left=getMaxDIff(root.left);
        int right=getMaxDIff(root.right);
        int localdiff=Math.max(root.data-left,root.data-right);
        int localmin=Math.min(root.data,Math.min(left,right));
        maxdiff=Math.max(maxdiff,localdiff);
        return localmin;
    }

}
