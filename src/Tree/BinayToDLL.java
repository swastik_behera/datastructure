package Tree;

public class BinayToDLL {
    private Node previous;
    Node bToDLL(Node root)
    {
        if(root==null) return null;
        Node head=bToDLL(root.left);
        if(previous==null)
            head=root;
        else{
            root.left=previous;
            previous.right=root;
        }
        previous=root;
        bToDLL(root.right);
        return head;
    }
}
