package Tree;

import java.util.ArrayList;

public class SerializeAndDeserialize {
    public static final int EMPTY=-1;
    public int index=0;
    public void serialize(Node root, ArrayList<Integer> A) {
        if(root==null)
        {
            A.add(EMPTY);
            return;
        }
        A.add(root.data);
        serialize(root.left,A);
        serialize(root.right,A);
    }

    public Node deSerialize(ArrayList<Integer> A){
            if(index==A.size())
                return null;
            int val=A.get(index);
            index++;
            if(val==EMPTY)
                return null;
            Node root=new Node(val);
            root.left=deSerialize(A);
            root.right=deSerialize(A);
            return root;
    }
}
