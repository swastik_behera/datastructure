package Tree;

public class MaxSmNonAdjacent {

    static int getMaxSum(Node root)
    {
        pair p=getSum(root);
        return p.sum;
    }

    private static pair getSum(Node root) {
        if(root==null)
        {
            return new pair();
        }
        pair left=getSum(root.left);
        pair right=getSum(root.right);
        pair returnpair=new pair();
        if(left.isChildConsidered==false && right.isChildConsidered==false
                && left.sum+ right.sum<left.sum+ right.sum+root.data){
            returnpair.sum=left.sum+ right.sum+root.data;
            returnpair.isChildConsidered=true;
        }
        else if(left.isChildConsidered==false && left.sum+root.data>left.sum+right.sum)
        {
            returnpair.sum=left.sum+root.data;
            returnpair.isChildConsidered=true;
        }
        else if(right.isChildConsidered==false && right.sum+root.data>left.sum+right.sum)
        {
            returnpair.sum=right.sum+root.data;
            returnpair.isChildConsidered=true;
        }

        else if(left.sum+ right.sum>root.data)
        {
            returnpair.sum=left.sum+right.sum;
            returnpair.isChildConsidered=false;
        }
        else {
            returnpair.sum=root.data;
            returnpair.isChildConsidered=true;
        }
        return returnpair;
    }
}
class pair{
    int sum;
    boolean isChildConsidered;
}
