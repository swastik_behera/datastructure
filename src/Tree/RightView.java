package Tree;

import java.util.LinkedList;
import java.util.Queue;

public class RightView {
    void rightView(Node node) {
        if(node==null) return;
        Queue<Node> q=new LinkedList<>();
        q.add(node);
        while(q.isEmpty()==false)
        {
            int count=q.size();
            for(int i=0;i<count;i++)
            {
                Node curr=q.poll();
                if(i==count-1)
                    System.out.print(curr.data+" ");
                if(curr.left!=null) q.add(curr.left);
                if(curr.right!=null) q.add(curr.right);
            }
        }
    }
}

