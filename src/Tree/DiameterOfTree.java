package Tree;

public class DiameterOfTree {
    int diameter=0;
    int diameter(Node root) {
        find(root);
        return diameter;
    }

    private int find(Node root) {
        if(root==null) return 0;
        int lh=find(root.left);
        int rh=find(root.right);
        diameter=Math.max(diameter,1+lh+rh);
        return 1+Math.max(lh,rh);
    }
}
