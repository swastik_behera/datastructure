package Tree;

public class MirrorTree {

    void mirror(Node root) {
        if (root == null) return;
        Node left = root.left;
        Node right = root.right;
        mirror(left);
        root.left = right;
        mirror(right);
        root.right = left;
    }
}
