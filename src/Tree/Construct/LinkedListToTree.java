package Tree.Construct;

import java.util.LinkedList;
import java.util.Queue;
public class LinkedListToTree {
    public static Tree convert(Node head, Tree root) {
        if(head==null) return root;
        Queue<Tree> q=new LinkedList<>();
        root=new Tree(head.data);
        q.add(root);
        Node curr=head.next;
        while(q.isEmpty()==false)
        {
            int count=q.size();
            for(int i=0;i<count;i++)
            {
                Tree pollNode=q.poll();
                if(curr!=null)
                {
                    Tree left=new Tree(curr.data);
                    pollNode.left=left;
                    q.add(left);
                    curr=curr.next;
                }
                if(curr!=null)
                {
                    Tree right=new Tree(curr.data);
                    pollNode.right=right;
                    q.add(right);
                    curr=curr.next;
                }
            }
        }
        return root;
    }
}
class Node{
    int data;
    Node next;
    Node(int d) {
        data = d;
        next = null;
    }
}
