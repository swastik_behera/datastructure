package Tree;

public class TreeFromInOrderPostOrder {

    int postIndex;

    public static void main(String[] args) {
        Node root=new TreeFromInOrderPostOrder().buildTree(new int[]{4 ,8 ,2 ,5 ,1 ,6 ,3 ,7},new int[]{8 ,4 ,5 ,2 ,6 ,7 ,3 ,1},8);
    }
    Node buildTree(int in[], int post[], int n) {
       postIndex=n-1;
       int instart=0;
       int inend=n-1;
      Node root= build(in,post,n,instart,inend);
      return root;
    }

    private Node build(int[] in, int[] post, int n, int instart, int inend) {
        if(instart>inend) return null;
        Node root=new Node(post[postIndex--]);
        int rootindex=-1;
        for(int i=instart;i<=inend;i++)
        {
            if(root.data==in[i]) {
                rootindex = i;
                break;
            }
        }
        root.right=build(in,post,n,rootindex+1,inend);
        root.left=build(in,post,n,instart,rootindex-1);
        return root;
    }
}
