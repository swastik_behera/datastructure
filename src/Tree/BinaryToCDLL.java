package Tree;

public class BinaryToCDLL {
    public static void main(String[] args) {
        Node root=new Node(1);
        root.left=new Node(3);
        root.right=new Node(2);
        new BinaryToCDLL().bTreeToClist(root);
    }
    Node previous;
    Node bTreeToClist(Node root)
    {
        if(root==null) return null;
        Node head=bTreeToClist(root.left);
        if(previous==null)
            head=root;
        else{
            root.left=previous;
            previous.right=root;
        }
        previous=root;
        bTreeToClist(root.right);
        if(head!=null) {
            previous.right = head;
            head.left = previous;
        }
        return head;
    }

}
