package Tree;

public class MaximumPathSum {
    public static void main(String[] args) {
        Node root=new Node(-2);
        root.left=new Node(-10);
       /* root.left.left=new Node(20);
        root.left.right=new Node(1);*/
        root.right=new Node(-90);
        root.right.left=new Node(3);
        root.right.right=new Node(4);
        int res=new MaximumPathSum().findMaxSum(root);
        System.out.println(res);
    }
    int maxsum=Integer.MIN_VALUE;
    int findMaxSum(Node node)
    {
        find(node);
        return maxsum;
    }
    int find(Node root)
    {
        if(root==null) return Integer.MIN_VALUE;
        int left=find(root.left);
        int right=find(root.right);
        int sidemax=Math.max(left,right);
        int retval=(sidemax==Integer.MIN_VALUE)?root.data:Math.max(root.data,root.data+sidemax);
        if(sidemax!=Integer.MIN_VALUE)
             sidemax=Math.max(sidemax,Math.max(root.data,root.data+sidemax));
        maxsum=(sidemax==Integer.MIN_VALUE)?Math.max(maxsum,root.data):Math.max(maxsum,Math.max(sidemax,root.data+left+right));
        return retval;
    }
}
