package Tree;

import java.util.ArrayList;

public class Inorder {

    ArrayList<Integer> inOrder(Node root)
    {
        ArrayList<Integer> list= new ArrayList<>();
        inorderlist(root,list);
        return list;
    }

    private void inorderlist(Node root, ArrayList<Integer> list) {
        if(root==null) return;
        inorderlist(root.left,list);
        list.add(root.data);
        inorderlist(root.right,list);
    }
}
