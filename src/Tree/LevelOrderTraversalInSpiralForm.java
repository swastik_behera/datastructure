package Tree;

import java.util.Deque;
import java.util.LinkedList;

public class LevelOrderTraversalInSpiralForm {
    void printSpiral(Node node)
    {
        if(node==null) return;
        Deque<Node> dq=new LinkedList<>();
        dq.add(node);
        boolean isEven=true;
        while(dq.isEmpty()==false)
        {
            int count=dq.size();
            for(int i=0;i<count;i++)
            {
                if(isEven)
                {

                    Node curr=dq.pollLast();
                    System.out.print(curr.data+" ");
                    if(curr.right!=null) dq.addFirst(curr.right);
                    if(curr.left!=null) dq.addFirst(curr.left);
                }
                else{
                    Node curr=dq.pollFirst();
                    System.out.print(curr.data+" ");
                    if(curr.left!=null) dq.addLast(curr.left);
                    if(curr.right!=null) dq.addLast(curr.right);
                }
            }
            isEven=!isEven;
        }
    }
}
