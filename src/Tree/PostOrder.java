package Tree;

import java.util.ArrayList;

public class PostOrder {

    ArrayList<Integer> postOrder(Node root)
    {
        ArrayList<Integer> list= new ArrayList<>();
        postOrderlist(root,list);
        return list;
    }

    private void postOrderlist(Node root, ArrayList<Integer> list) {
        if(root==null) return;
        postOrderlist(root.left,list);
        postOrderlist(root.right,list);
        list.add(root.data);
    }
}
