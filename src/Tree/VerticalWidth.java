package Tree;

public class VerticalWidth {

    public static int verticalWidth(Node root)
    {
        if(root==null) return 0;
        Vertical v=new Vertical();
        find(root,v,0);
        return Math.abs(v.leftline-v.rightline)+1;
    }

    private static void find(Node root, Vertical v, int currentline) {
        if(root==null) return;
        if(currentline<v.leftline)
            v.leftline--;
        if(currentline>v.rightline)
            v.rightline++;
        find(root.left,v,currentline-1);
        find(root.right,v,currentline+1);
    }

}
class Vertical{
    int leftline;
    int rightline;
}
