package Tree;

public class CheckIfBalanced {

    boolean isBalanced(Node root)
    {
        int res= checkBalance(root);
        if(res>=0) return true;
        return false;
    }

    private int checkBalance(Node root) {
        if(root==null) return 0;
        int lh=checkBalance(root.left);
        int rh=checkBalance(root.right);
        if(lh<0 || rh<0 ) return -1;
        int diff=Math.abs(lh-rh);
        if(diff>1) return -1;
        else return 1+Math.max(lh,rh);
    }
}
