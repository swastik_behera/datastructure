package Tree;

import java.util.ArrayList;

public class PreOrder {

    static ArrayList<Integer> preorder(Node root)
    {
        ArrayList<Integer> list= new ArrayList<>();
        preorderlist(root,list);
        return list;
    }

    private static void preorderlist(Node root, ArrayList<Integer> list) {
        if(root==null) return;
        list.add(root.data);
        preorderlist(root.left,list);
        preorderlist(root.right,list);
    }

}
