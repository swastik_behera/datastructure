package BinarySearchTree;

public class Ceil {
    int findCeil(Node root, int key) {
        if (root == null)
            return -1;
        int res=-1;
        while(root!=null)
        {
            if(root.data==key)
                return root.data;
            else if(root.data>key){
                res=root.data;
                root=root.left;
            }
            else root=root.right;
        }
        return res;
    }
}
