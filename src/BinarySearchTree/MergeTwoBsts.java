package BinarySearchTree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class MergeTwoBsts {
    public static void main(String[] args) {
        Node root2=new Node(5);
        root2.left=new Node(3);
        root2.right=new Node(6);
        root2.left.left=new Node(2);
        root2.left.right=new Node(4);
        Node root1=new Node(2);
        root1.left=new Node(1);
        root1.right=new Node(3);
        root1.right.right=new Node(7);
        root1.right.right.left=new Node(6);
        for(int x:new MergeTwoBsts().merge(root1,root2))
            System.out.print(x+" ");
    }
    public List<Integer> merge(Node root1, Node root2) {
        List<Integer> list = new ArrayList<>();
        Stack<Node> st1 = new Stack<>();
        Stack<Node> st2 = new Stack<>();
        Node curr1 = root1;
        Node curr2 = root2;
        if(root1==null) {
            inorder(root2,list);
            return list;
        }
        if(root2==null)
        {
            inorder(root1,list);
            return list;
        }
        while (true) {
            while (curr1 != null) {
                st1.push(curr1);
                curr1 = curr1.left;
            }
            while (curr2 != null) {
                st2.push(curr2);
                curr2 = curr2.left;
            }
            if(st1.isEmpty()==false && st2.isEmpty()==false) {
                curr1 = st1.peek();
                curr2 = st2.peek();
                if (curr1.data == curr2.data) {
                    list.add(curr1.data);
                    list.add(curr2.data);
                    st1.pop();
                    st2.pop();
                    curr1=curr1.right;curr2=curr2.right;
                } else if (curr1.data < curr2.data) {
                    list.add(curr1.data);
                    st1.pop();
                    curr1 = curr1.right;
                    curr2 = null;
                } else {
                    list.add(curr2.data);
                    st2.pop();
                    curr2 = curr2.right;
                    curr1 = null;
                }
            }
            else if(st1.isEmpty())
            {
                while(st2.isEmpty()==false) {
                    curr2 = st2.pop();
                    curr2.left = null;
                    inorder(curr2, list);
                }
                return list;

            }
            else if(st2.isEmpty())
            {
                while(st1.isEmpty()==false) {
                    curr1 = st1.pop();
                    curr1.left = null;
                    inorder(curr1, list);
                }
                return list;
            }
            else break;
        }
        return list;
    }

    private void inorder(Node root, List<Integer> list) {
        if(root==null) return;
        inorder(root.left,list);
        list.add(root.data);
        inorder(root.right,list);
    }
}
