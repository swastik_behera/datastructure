package BinarySearchTree;

public class FixTwoNOde {
    public static void main(String[] args) {
        Node root=new Node(10 );
        root.left=new Node(5);
        root.right=new Node(8);
        root.left.left=new Node(2);
        root.left.right=new Node(20);
        new FixTwoNOde().correctBST(root);
    }
    Node first=null;
    Node second=null;
    Node previous =null;
    public Node correctBST(Node root)
    {
        findTwoNode(root);
        int temp=first.data;
        first.data=second.data;
        second.data=temp;
        return root;
    }

    public void findTwoNode(Node root) {
        if(root==null) return ;
        findTwoNode(root.left);
        if(previous!=null && root.data<previous.data){
            if(first==null){
                first=previous;
                second=root;
            }
            else second=root;
        }
        previous =root;
        findTwoNode(root.right);
    }
}
