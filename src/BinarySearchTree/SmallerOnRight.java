package BinarySearchTree;

import org.omg.CORBA.INTERNAL;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.TreeSet;

public class SmallerOnRight {
    public static void main(String[] args) throws Exception{
        BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
        int t=Integer.parseInt(br.readLine());
        while(t-->0)
        {
            int n=Integer.parseInt(br.readLine());
            String[] input=br.readLine().split(" ");
            System.out.println(getCount(input,n));
        }
    }

    private static int getCount(String[] input,int n) {
        int count=0;
        TreeSet<Integer> set=new TreeSet<>();
        for(int i=n-1;i>=0;i--)
        {
            int num=Integer.parseInt(input[i]);
            set.add(num);
            count=Math.max(count,set.headSet(num).size());
        }
        return count;
    }


}
class Count{
    int smallercount;
}
