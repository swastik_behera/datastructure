package BinarySearchTree;

public class CheckForBST {
    boolean isBST(Node root)
    {
        int min=Integer.MIN_VALUE;
        int max=Integer.MAX_VALUE;
        return find(root,min,max);

    }

    private boolean find(Node root, int min, int max) {
        if(root==null) return true;
        return root.data>min && root.data<max
                && find(root.left,min,root.data)
                && find(root.right,root.data,max);
    }
}
