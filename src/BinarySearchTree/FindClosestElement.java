package BinarySearchTree;

public class FindClosestElement {
    static int maxDiff(Node  root, int K)
    {
        int diff=Integer.MAX_VALUE;
        while(root!=null)
        {
            int localdiff=Math.abs(root.data-K);
            if(localdiff<diff)
                diff=localdiff;
            if(root.data>K)
                root=root.left;
            else if(root.data<K)
                root=root.right;
            else
                return diff;
        }
        return diff;
    }


}
