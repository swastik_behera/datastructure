package BinarySearchTree;

public class Floor {
    public static void main(String[] args) {
        Node root=new Node(4);
        root.left=new Node(2);
        root.right=new Node(15);
        root.left.left=new Node(1);
        System.out.println(new Floor().floor(root,19));
    }

    int floor(Node root, int key)
    {
        if(root==null) return -1;
        if(root.data>key)
            return floor(root.left,key);
        else if(root.data<key)
        {
            int curval=root.data;
            int retval=floor(root.right,key);
            if(retval!=-1)
                return Math.max(curval,retval);
            return curval;
        }
        return root.data;
    }
}
