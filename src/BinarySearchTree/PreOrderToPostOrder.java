package BinarySearchTree;

import java.util.Arrays;

public class PreOrderToPostOrder {
    int  index=0;
    public static Node constructTree(int pre[], int size) {

        int[] in= Arrays.copyOf(pre,pre.length);
        Arrays.sort(in);
        Node root=new PreOrderToPostOrder().construct(pre,in,0,size-1);
        return root;
    }

    private  Node construct(int[] pre, int[] in, int start, int end) {
        if(start>end) return null;
        int val=pre[index++];
        Node root=new Node(val);
        int findindex=-1;
        for(int i=start;i<=end;i++)
        {
            if(in[i]==val) {
                findindex = i;
                break;
            }
        }
        root.left=construct(pre,in,start,findindex-1);
        root.right=construct(pre,in,findindex+1,end);
        return root;
    }


}
