package BinarySearchTree;

import java.util.ArrayList;
import java.util.Stack;

public class PrintCommon {
    public static ArrayList<Integer> printCommon(Node root1, Node root2)
    {
        ArrayList<Integer> list=new ArrayList<>();
        Stack<Node> st1=new Stack<>();
        Stack<Node> st2=new Stack<>();
        while(true)
        {
            if(root1!=null)
            {
                st1.push(root1);
                root1=root1.left;
            }
            if(root2!=null)
            {
                st2.push(root2);
                root2=root2.left;
            }
            else if(st1.isEmpty()==false && st2.isEmpty()==false)
            {
                root1=st1.peek();
                root2=st2.peek();
                if(root1.data==root2.data)
                {
                    list.add(root1.data);
                    st1.pop();
                    st2.pop();
                    root1=root1.right;
                    root2=root2.right;
                }
                else if(root1.data<root2.data)
                {
                    st1.pop();
                    root1=root1.right;
                    root2=null;
                }
                else if(root1.data>root2.data)
                {
                    st2.pop();
                    root2=root2.right;
                    root1=null;
                }
            }
            else break;
        }
        return list;
    }



}
