package BinarySearchTree;

import java.util.HashSet;
import java.util.Stack;

public class FindPair {
    static boolean findPair(Node root, int sum) {
        if(root==null) return false;
        Stack<Node> stack=new Stack<>();
        HashSet<Integer> set=new HashSet<>();
        Node curr=root;
        while(curr!=null || stack.isEmpty()==false)
        {
            while (curr!=null)
            {
                stack.push(curr);
                curr=curr.left;
            }
            curr=stack.pop();
            if(set.contains(sum-curr.data))
                return true;
            set.add(curr.data);
            curr=curr.right;
        }
        return false;
    }
}
