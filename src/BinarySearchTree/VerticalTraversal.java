package BinarySearchTree;

import java.util.*;

public class VerticalTraversal {
    static ArrayList<Integer> verticalOrder(Node root)
    {
        Map<Integer,List<Integer>> map=new TreeMap<>();
        ArrayList<Integer> res=new ArrayList<>();
        if(root==null) return res;
        Queue<Pair> q=new LinkedList<>();
        q.add(new Pair(root,0));
        while(q.isEmpty()==false)
        {
            Pair p=q.poll();
            Node curr=p.node;
            int hd=p.hd;
            if(map.containsKey(hd))
                map.get(hd).add(curr.data);
            else {
                LinkedList<Integer> list=new LinkedList<>();
                list.add(curr.data);
                map.put(hd,list);
            }
            if(curr.left!=null) q.add(new Pair(curr.left,hd-1));
            if(curr.right!=null) q.add(new Pair(curr.right,hd+1));
        }
        for(Map.Entry<Integer,List<Integer>> entry:map.entrySet())
        {
            for(int x:entry.getValue())
               res.add(x);
        }
        return res;
    }


}
class Pair{
    Node node;
    int hd;
    Pair(Node node,int hd)
    {
        this.node=node;
        this.hd=hd;
    }
}
