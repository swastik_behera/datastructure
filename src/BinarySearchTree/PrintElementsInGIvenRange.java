package BinarySearchTree;

import java.util.ArrayList;
import java.util.Stack;

public class PrintElementsInGIvenRange {

    public static ArrayList<Integer> printNearNodes(Node root, int low, int high)
    {
        ArrayList<Integer> list = new ArrayList<>();
        if(root==null) return list;
        Stack<Node> stack=new Stack<>();
        Node curr=root;
        while(curr!=null || stack.isEmpty()==false)
        {
            while(curr!=null)
            {
                stack.push(curr);
                curr=curr.left;
            }
            curr=stack.pop();
            if(curr.data>=low && curr.data<=high)
                list.add(curr.data);
            curr=curr.right;
        }
        return list;
    }
}
