package BinarySearchTree;

public class GetMinimum {
    int minimumVal=Integer.MIN_VALUE;
    int minValue(Node node)
    {
        if(node==null) return -1;
        getmin(node);
        return minimumVal;

    }

    private void getmin(Node node) {
        if(node==null) return;
        minimumVal=node.data;
        getmin(node.left);
    }
}
