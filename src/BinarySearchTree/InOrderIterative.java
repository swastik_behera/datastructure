package BinarySearchTree;

import java.util.Stack;

public class InOrderIterative {

    public static void inOrder(Node root)
    {
        if(root==null)
            return;
        Stack<Node> st=new Stack<>();
        Node curr=root;
        while(curr!=null && st.isEmpty()==false)
        {
            while(curr!=null)
            {
                st.push(curr);
                curr=curr.left;
            }
            curr=st.pop();
            System.out.println(curr.data+" ");
            curr=curr.right;
        }

    }
}
