package BinarySearchTree;

public class DeleteNode {
    public static Node deleteNode(Node root, int X)
    {
        if(root==null) return root;
        else if(root.data>X)
            root.left=deleteNode(root.left,X);
        else if(root.data<X)
            root.right=deleteNode(root.right,X);
        else{
            if(root.left==null) return root.right;
            else if(root.right==null) return root.left;
            else{
                Node nextsucc=findsuccesor(root.right);
                root.data=nextsucc.data;
                root.right=deleteNode(root.right,nextsucc.data);
            }
        }
        return root;
    }

    private static Node findsuccesor(Node right) {

        while(right!=null && right.left!=null)
                right=right.left;
        return right;
    }
}
