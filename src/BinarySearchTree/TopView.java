package BinarySearchTree;

import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

public class TopView {
    static void topView(Node root)
    {
        Queue<Pair> q=new LinkedList<>();
        Map<Integer,Integer> map=new TreeMap<>();
        if(root==null) return;
        q.add(new Pair(root,0));
        while(q.isEmpty()==false)
        {
            Pair p=q.poll();
            Node curr=p.node;
            int hd=p.hd;
            if(map.containsKey(hd)==false)
                map.put(hd,curr.data);
            if(curr.left!=null) q.add(new Pair(curr.left,hd-1));
            if(curr.right!=null) q.add(new Pair(curr.right,hd+1));
        }
        for(Map.Entry<Integer,Integer> entry:map.entrySet())
            System.out.print(entry.getValue()+" ");

    }
}

