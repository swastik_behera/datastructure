package BinarySearchTree;

import java.util.LinkedList;
import java.util.Queue;

public class ConstructBinaryTreeFromLevelOrder {
    public static void main(String[] args) {
        Node root=new ConstructBinaryTreeFromLevelOrder().constructBST(new int[]{7 ,4 ,12, 3, 6, 8, 1, 5, 10});
    }
    public Node constructBST(int[] arr){
        Queue<Node> q=new LinkedList<>();
        int i=0;
        int n=arr.length;
        Node root= new Node(arr[i++]);
        q.add(root);
        while(q.isEmpty()==false)
        {
            Node curr=q.poll();
            if(i<n)
            {
                Node left=new Node(arr[i]);
                curr.left=left;
                q.add(left);
                i++;
            }
            if(i<n){
                Node right=new Node(arr[i]);
                curr.right=right;
                q.add(right);
                i++;
            }
        }
        return root;
    }
}
