package Strings;

public class SmallestWindow {
    public static void main(String[] args) {
        System.out.println(smallestWindow("timetopractice","toc"));
    }

    public static String smallestWindow(String str, String pat){
        int slength=str.length();
        int plength=pat.length();
        if(plength>slength) return "-1";
        int[] count=new int[256];
        for(char c:pat.toCharArray())
            count[c]++;
        int i=0,r=0,l=0,j=0,missing=plength;
        while(r<slength)
        {
            if(count[str.charAt(r)]>0)
                missing--;
            count[str.charAt(r)]--;
            r=r+1;
            while(missing==0)
            {
                if(j==0 || r-l<j-i)
                {
                    i=l;j=r;
                }
                count[str.charAt(l)]++;
                if(count[str.charAt(l)]>0) missing++;
                l=l+1;
            }
        }
        String result=str.substring(i,j);
        if(result.length()==0) return "-1";
        return result;
    }
}
