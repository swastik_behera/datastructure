package Strings;
/*Given a string check if it is Pangram or not.
        A pangram is a sentence containing every letter in the English Alphabet.*/

public class Pangram {
    public static void main(String[] args) {
        System.out.println(checkPanagram("sdfs"));
    }
    public static boolean checkPanagram  (String s)
    {
        s=s.toLowerCase();
        int start='a'+0;
        int end='z'+0;
        int[] count=new int[256];
        for(char c:s.toCharArray())
            count[c]++;
        for(int i=start;i<=end;i++)
        {
            if(count[i]==0)
                return false;
        }
        return true;
    }
}
