package Strings;

public class Anagram {
    public static void main(String[] args) {
        isAnagram("geeksforgeeks", "forgeeksgeeks");
    }
    public static boolean isAnagram(String s1,String s2)
    {
        int n=s1.length();
        int m=s2.length();
        if(n!=m) return false;
        int[] count=new int[256];
        for(char c:s1.toCharArray())
            count[c]++;
        for(char c:s2.toCharArray())
            count[c]--;
        int i=0;
        while(i<256)
        {
            if(count[i++]!=0) return false;
        }
        return true;
    }
}
