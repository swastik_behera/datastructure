package Strings;

public class MinIndexedCharacter {
    public static void main(String[] args) {
       solution("geeksforgeeks","abc");
    }
    public static void solution(String s1, String s2){
        int result=Integer.MAX_VALUE;
        char c='\0';
        for(int i=0;i<s2.length();i++)
        {
            int index=s1.indexOf(s2.charAt(i));
            if(index!=-1 && index<result)
            {
                result=index;
                c=s2.charAt(i);
            }
        }
        if(result==Integer.MAX_VALUE) {
            System.out.println("No character present");
            return;
        }
        System.out.println(c);
    }
}
