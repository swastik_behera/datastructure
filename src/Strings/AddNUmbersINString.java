package Strings;

public class AddNUmbersINString {
    public static void main(String[] args) {
        System.out.println(findSum("1abc2x30yz67"));

    }
    public static long findSum(String s){
        long sum=0;
        int n=s.length();
        int i=0;
        int start='0'+0;
        int end='9'+0;
        while(i<n)
        {
            int number=0;
            while(i<n && s.charAt(i)+0>=start && s.charAt(i)<=end)
            {
                number=number*10+Integer.parseInt(""+s.charAt(i));
                i++;
            }
            if(number!=0)
            sum=sum+number;
            i++;
        }
        return sum;
    }
}
