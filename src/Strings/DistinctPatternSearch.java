package Strings;

public class DistinctPatternSearch {
    static boolean search(String pat, String txt)
    {
       int m=pat.length();
       int n=txt.length();
       int j=0;
       int i=0;
       while(i<=n-m)
       {
           for(j=0;j<m;j++)
           {
               if(pat.charAt(j)!=txt.charAt(i+j))
                   break;
           }
           if(j==0) i++;
           if(j==m) return  true;
           else i=i+j;
       }
       return false;
    }
}
