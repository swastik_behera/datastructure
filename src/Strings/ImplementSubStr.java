package Strings;

public class ImplementSubStr {
    int strstr(String str, String target)
    {
        int n=str.length();
        int m=target.length();
        for(int i=0;i<=n-m;i++)
        {
            int j=0;
            for(j=0;j<m;j++){
                if(str.charAt(i+j)!=target.charAt(j))
                    break;
            }
            if(j==m) return i;
        }
        return -1;
    }
}
