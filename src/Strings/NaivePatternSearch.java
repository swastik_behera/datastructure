package Strings;
/*Given a string S and a pattern P both of lowercase characters.
        The task is to check if the given pattern exists in the given string or not.*/

public class NaivePatternSearch {
    static boolean search(String pat, String txt)
    {
        if(pat.length()==0) return false;
        int n=txt.length();
        int m=pat.length();

        for(int i=0;i<=n-m;i++)
        {
            int j=0;
            for(j=0;j<m;j++)
            {
                if(txt.charAt(i+j)!=pat.charAt(j))
                    break;
            }
            if(j==m)
                return true;
        }
        return false;
    }

}
