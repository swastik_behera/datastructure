package Strings;

public class CaseSpecificSorting {
    public static void main(String[] args) {
        System.out.println(caseSort("srbDKi"));
    }
    public static String caseSort(String str) {
        String result="";
        int[] count=new int[256];
        for(char c:str.toCharArray())
            count[c]++;
        int capsstart='A'+0;
        int capend='Z'+0;
        int smallstart='a'+0;
        int smallend='z'+0;
        int capcounter=capsstart;
        int smallcounter=smallstart;
        for(char c:str.toCharArray())
        {
            if(c+0<=capend && c+0>=capsstart)
            {
                while(true)
                {
                    if(count[capcounter]>0) {
                        result += (char) capcounter;
                        count[capcounter]--;
                        break;
                    }
                    else capcounter++;
                }
            }
            else{
                while(true)
                {
                    if(count[smallcounter]>0) {
                        result += (char) smallcounter;
                        count[smallcounter]--;
                        break;
                    }
                    else smallcounter++;
                }
            }
        }
        return result;
    }

}
