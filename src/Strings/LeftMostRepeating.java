package Strings;

public class LeftMostRepeating {
    static int repeatedCharacter(String str)
    {
        int[] fi=new int[256];
        int result=Integer.MAX_VALUE;
        int n=str.length();
        for(int i=n-1;i>=0;i--)
        {
            if(fi[str.charAt(i)]!=0)
                result=Math.min(result,i);
            else fi[str.charAt(i)]=1;
        }
        return result==Integer.MAX_VALUE?-1:result;
    }
}
