package Strings;

public class MaxRepeatingChar {
    public static void main(String[] args) {
        getMaxOccuringChar("testsample");
    }
    public static char getMaxOccuringChar(String line){
        int[] count=new int[256];
        int result=0;
        int frequency=0;
        for(char c:line.toCharArray())
            count[c]++;
        for(int i=0;i<256;i++)
        {
            if(count[i]>frequency) {
                frequency=count[i];
                result = i;
            }
        }
        return (char)result;
    }
}
