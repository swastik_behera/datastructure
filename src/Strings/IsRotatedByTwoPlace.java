package Strings;

public class IsRotatedByTwoPlace {
    public static void main(String[] args) {
        System.out.println(isRotated("fsbcnuvqhffbsaqxwp","wpfsbcnuvqhffbsaqx"));

    }
    public static boolean isRotated(String s1, String s2){
        if(s1.length()>2) {
            String part1 = s1.substring(0, 2);
            String part2 = s1.substring(s1.length() - 2, s1.length());
            String s3 = s1 + part1;
            int n = s3.length();
            s3 = s3.substring(2, n);
            if (s2.equals(s3))
                return true;
            s3 = part2 + s1;
            s3 = s3.substring(0, n - 2);
            if (s2.equals(s3))
                return true;
        }
        else{
            if(s1.equals(s2))
            return true;
        }
        return false;
    }
}
