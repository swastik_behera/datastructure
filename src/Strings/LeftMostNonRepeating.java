package Strings;

import java.util.Arrays;

public class LeftMostNonRepeating {
    public static void main(String[] args) {
        nonrepeatingCharacter("hello");
    }
    static char nonrepeatingCharacter(String str)
    {
        int[] fi =new int[256];
        Arrays.fill(fi,-1);
        for(int i=0;i<str.length();i++)
        {
            if(fi[str.charAt(i)]==-1)
                fi[str.charAt(i)]=i;
            else fi[str.charAt(i)]=-2;
        }
        int result=Integer.MAX_VALUE;
        for(int i=0;i<256;i++)
            if(fi[i]>=0)
            {
                result=Math.min(result,fi[i]);
            }
        return result==Integer.MAX_VALUE?'$':str.charAt(result);
    }
}
