package Strings;

public class isRotationOfEachOther {
    public static void main(String[] args) {
        System.out.println(areRotations("geeksforgeeks","forgeeksgeeks"));
    }
    public static boolean areRotations(String s, String x )
    {
        if(s.length()!=x.length())
            return false;
        s=s+s;
        int ret=s.indexOf(x);
        if(x.indexOf(s)!=-1)
            return true;
        return false;
    }
}
